package com.expresionless.jokeapp;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

/**
 * Allows the user to search for a joke that comes from the web.
 *
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class WebSearch extends Activity 
{
	private EditText shortDescription;
	private Spinner categorySpinner;
	private Spinner subCategorySpinner;

	private ArrayAdapter<CharSequence> geekAdapter;
	private ArrayAdapter<CharSequence> workAdapter;
	private ArrayAdapter<CharSequence> sportsAdapter;
	private static final String TAG = "NetStat";
	@SuppressWarnings("unused")
	private ImageView imageView;
	private ArrayList<String> jokes;
	private ArrayList<String> descriptions;
	// this is how we get our resutls to our other view
	private Bundle jokesBundle;
	private Context context;
	private String radioSelected = "1";
	private ProgressBar bar;
	private int progress;
	@SuppressWarnings("unused")
	private int fileLength;

	/**
	 * Creates the activity and sets the layout.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.web_search);
		jokesBundle = new Bundle();
		context = this;
		jokes = new ArrayList<String>();
		descriptions = new ArrayList<String>();
		inflateComponents();
		checkConnection();
	}

	/**
     * Event handler that deals with the act of searching for a joke.
     *
     * @param v
     */
	public void onSearchClick(View v) {
		if(validateForm())
		{
			try 
			{
				new GetWebData().execute(categorySpinner.getSelectedItem()
						.toString(), subCategorySpinner.getSelectedItem()
						.toString(), radioSelected, shortDescription.getText()
						.toString());
			} 
			catch (Exception e)
			{
				Log.d(TAG, "Exception:" + e.getMessage());
			}
		}
	}
	
	/**
     * Checks the status of the connection.
     */
	private void checkConnection() {
		// get an instance of ConnectivityManager
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		// get an instance of NetworkInfo for wifi.
		NetworkInfo networkInfo = connMgr
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		// check if wifi is available
		boolean isWifiConn = networkInfo.isAvailable();

		// get an instance of NetworkInfo for mobile
		networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		// Check if mobile is available
		boolean isMobileConn = networkInfo.isAvailable();

		Toast.makeText(this, "Connection : " + isMobileConn, Toast.LENGTH_LONG)
				.show();
		Log.d(TAG, "Wifi available: " + isWifiConn);
		Log.d(TAG, "Mobile available: " + isMobileConn);
	}

	/**
     * Locates the data based on the select query sent in from the PHP located
     * on the server.
     *
     * @param matchKey
     * @return
     * @throws MalformedURLException
     * @throws IOException
     */
	public String findData(String[] matchKey) throws MalformedURLException,
			IOException {
		String newFeed = "http://waldo2.dawsoncollege.qc.ca/1011681/php/clientServerConnection.php?category="
				+ matchKey[0]
				+ "&amp&subCategory="
				+ matchKey[1]
				+ "&amp&typeCode="
				+ matchKey[2]
				+ "&amp&shortDescription="
				+ matchKey[3];

		StringBuilder response = new StringBuilder();

		URL url = new URL(newFeed);

		HttpURLConnection httpconn = (HttpURLConnection) url.openConnection();
		httpconn.connect();
		fileLength = httpconn.getContentLength();
		if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
			InputStream in = new BufferedInputStream(httpconn.getInputStream());

			BufferedReader input = new BufferedReader(new InputStreamReader(in));
			String strLine = null;
			while ((strLine = input.readLine()) != null)
				response.append(strLine);

			if (input != null)
				input.close();

			if (httpconn != null)
				httpconn.disconnect();
		}

		System.out.println(response.toString());

		return response.toString();
	}

	/**
     * Processes the received JSON response and analyzes the values contained
     * within it.
     *
     * @param resp
     * @throws IllegalStateException
     * @throws IOException
     * @throws JSONException
     * @throws NoSuchAlgorithmException
     */
	public void processJSONResponse(String resp) throws IllegalStateException,
			IOException, JSONException, NoSuchAlgorithmException {
		// [] wrapped in array
		JSONArray array = new JSONArray(resp);
		if (!(resp == null)) 
		{
			JSONObject jobj;
			
			for (int i = 0; i < array.length(); i++) 
			{
				Log.d(TAG, i + array.get(i).toString());
				// unnamed objects in array: [ {}, {}, {} ... ]
				jobj = array.getJSONObject(i);

				// named items in array
				if (jobj.has("_id")) {
					Log.d(TAG, i + ": " + jobj.getString("_id"));

					// Collection attached to the ListView
					jokes.add(i, jobj.getString("_id"));
				}

				if (jobj.has("shortDescription"))
					descriptions.add(i, jobj.getString("shortDescription"));
			}
		}
	}

	/**
     * Inner class used to get the data from the web - such as the JSON
     *
     * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
     */
	private class GetWebData extends AsyncTask<String, Integer, String> 
	{
		@Override
		protected void onPreExecute()
		{
			bar.setVisibility(ProgressBar.VISIBLE);
			progress = 0;
		}

		/**
		 * Background task that does logging and publishes the progress.
		 */
		protected String doInBackground(String... matchKey) 
		{
			Log.d(TAG, " category:" + matchKey[0]);
			Log.d(TAG, " sub-category:" + matchKey[1]);
			Log.d(TAG, " type code:" + matchKey[2]);
			Log.d(TAG, " short description:" + matchKey[3]);
			
			try 
			{
				// send the data to onPostExecute()
				publishProgress(progress);
				return findData(matchKey);
			} 
			catch (Exception e)
			{
				Log.d(TAG, "doInBackground() Exception:" + e.getMessage());
				e.printStackTrace();
				return "";
			}
		}

		/**
		 * Updates the progress bar.
		 */
		@Override
		protected void onProgressUpdate(Integer...progress) {
			// setting progress percentage
			bar.setProgress(progress[0]);
		}

		/**
		 * Takes care of processing the JSON response and fires the WebResults
		 * activity.
		 */
		protected void onPostExecute(String result) {
			try {
				// data from the HTTP connection stream is in JSON format
				bar.setVisibility(ProgressBar.INVISIBLE);
				processJSONResponse(result);

				// once JSON is parsed, we send our data through our intent with
				// the help of a bundle
				Intent intent = new Intent(context, WebResults.class);
				jokesBundle.putStringArrayList("id", jokes);
				jokesBundle.putStringArrayList("descriptions", descriptions);
				intent.putExtra("results", jokesBundle);
				startActivity(intent);

			} catch (Exception e) {
				Log.d(TAG, "onPostExecute() Exception:" + e.getMessage());
				e.printStackTrace();
			}

		}
	}

	/**
	 * Inflate the components from the form.
	 */
	private void inflateComponents() {
		shortDescription = (EditText) findViewById(R.id.short_desc_search_txt);
		imageView = (ImageView) findViewById(R.id.downloadedImage);

		categorySpinner = (Spinner) findViewById(R.id.cateogry_fuck);
		ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter
				.createFromResource(this, R.array.category_array,
						android.R.layout.simple_spinner_item);
		categoryAdapter
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		categorySpinner.setAdapter(categoryAdapter);
		subCategorySpinner = (Spinner) findViewById(R.id.subCategory_spinner);

		sportsAdapter = ArrayAdapter.createFromResource(this,
				R.array.Sport_array, android.R.layout.simple_spinner_item);
		sportsAdapter
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		workAdapter = ArrayAdapter.createFromResource(this, R.array.Work_array,
				android.R.layout.simple_spinner_item);
		workAdapter
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		geekAdapter = ArrayAdapter.createFromResource(this, R.array.Geek_array,
				android.R.layout.simple_spinner_item);
		geekAdapter
		.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		bar = (ProgressBar) findViewById(R.id.progressBar1);
		bar.setVisibility(ProgressBar.INVISIBLE);
		progress = 0;
		categorySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				if (position == 0)
					subCategorySpinner.setAdapter(geekAdapter);

				else if (position == 1)
					subCategorySpinner.setAdapter(sportsAdapter);

				else
					subCategorySpinner.setAdapter(workAdapter);
			}

			// do nothing method stub
			@Override
			public void onNothingSelected(AdapterView<?> parentView) { }
		});

	}

	/**
	 * Event handler used to alter the value of the radio button.
	 *
	 * @param view
	 */
	public void radioClicked(View view)
	{
		boolean checked = ((RadioButton) view).isChecked();

		switch (view.getId())
		{
			case R.id.radio_QA:
				if (checked)
					radioSelected = "1";
				break;
			case R.id.radio_mono:
				if (checked)
					radioSelected = "2";
				break;
			case R.id.radio_image:
				if (checked)
					radioSelected = "3";
		}
	}

	/**
	 * Performs data validation for the form.
	 */
	private boolean validateForm()
	{
		String shortDesc = shortDescription.getText().toString();
		shortDesc = shortDesc.trim();

		if (shortDesc.isEmpty())
		{
			Toast.makeText(this, "ERROR. Please fill out all fields.",
					Toast.LENGTH_LONG).show();
			return false;
		}
		
		else
			return true;
	}
}