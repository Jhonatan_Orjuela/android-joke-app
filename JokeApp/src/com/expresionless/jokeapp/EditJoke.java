package com.expresionless.jokeapp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

/**
 * Activity used to allow the user to edit a joke.
 * 
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class EditJoke extends Activity {
	private ArrayList<EditText> txtFields;

	private Spinner categorySpinner;
	private Spinner subCategorySpinner;
	private Spinner ratingSpinner;
	private Spinner booleanSpinner;
	private ArrayAdapter<CharSequence> geekAdapter;
	private ArrayAdapter<CharSequence> workAdapter;
	private ArrayAdapter<CharSequence> sportsAdapter;

	private RadioGroup typeGrp;
	private RadioButton typeQA;
	private RadioButton typeMono;
	private EditText shortDescription;
	private EditText jokeQuestion;
	private EditText jokeAnswer;
	private EditText monologue;
	private EditText comments;
	private EditText source;

	private TextView jokeQlbl;
	private TextView jokeAnslbl;
	private TextView jokeMonolbl;

	private static final String PREFS_NAME = "PrefsFile";
	private String joke;
	private JokeDBHelper jokeDBHelper;
	private SharedPreferences.Editor editor;
	private SharedPreferences prefs;
	private Cursor jokeInfo;

	/**
	 * Creates the activity and sets the layout.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_joke);

		jokeDBHelper = new JokeDBHelper(this);
		prefs = getSharedPreferences(PREFS_NAME, 0);
		editor = prefs.edit();
		joke = prefs.getString("Joke", "nothing");
		jokeInfo = jokeDBHelper.getJokeByShortDescription(joke);
		jokeInfo.moveToFirst();

		txtFields = new ArrayList<EditText>();
		inflateSpinners();
		inflateFormComponents();
	}

	/**
	 * Inflates the form components.
	 */
	private void inflateFormComponents() {
		shortDescription = (EditText) findViewById(R.id.shortDescEdit);
		shortDescription.setText(jokeInfo.getString(jokeInfo
				.getColumnIndex("shortDescription")));
		txtFields.add(shortDescription);

		jokeQuestion = (EditText) findViewById(R.id.jokeQuestEdit);
		jokeQuestion.setText(jokeInfo.getString(jokeInfo
				.getColumnIndex("question")));
		txtFields.add(jokeQuestion);

		jokeQlbl = (TextView) findViewById(R.id.jokeQuestLabel);

		jokeAnswer = (EditText) findViewById(R.id.jokeAnsEdit);
		jokeAnswer
				.setText(jokeInfo.getString(jokeInfo.getColumnIndex("answer")));
		txtFields.add(jokeAnswer);

		jokeAnslbl = (TextView) findViewById(R.id.jokeAnslbl);

		monologue = (EditText) findViewById(R.id.monologueEdit);
		monologue.setText(jokeInfo.getString(jokeInfo
				.getColumnIndex("monologue")));
		txtFields.add(monologue);
		jokeMonolbl = (TextView) findViewById(R.id.monologueLbl);

		comments = (EditText) findViewById(R.id.commentsEdit);
		comments.setText(jokeInfo.getString(jokeInfo.getColumnIndex("comments")));
		txtFields.add(comments);

		source = (EditText) findViewById(R.id.sourceEdit);
		source.setText(jokeInfo.getString(jokeInfo.getColumnIndex("jokeSource")));
		txtFields.add(source);

		typeGrp = (RadioGroup) findViewById(R.id.radio_grp);

		typeQA = (RadioButton) findViewById(R.id.radio_QA);
		typeMono = (RadioButton) findViewById(R.id.radio_mono);

		if (jokeInfo.getInt(jokeInfo.getColumnIndex("typeCode")) == 1) {
			monologue.setVisibility(View.GONE);
			jokeMonolbl.setVisibility(View.GONE);
			jokeQlbl.setVisibility(View.VISIBLE);
			jokeAnslbl.setVisibility(View.VISIBLE);
			jokeQuestion.setVisibility(View.VISIBLE);
			jokeAnswer.setVisibility(View.VISIBLE);
			typeQA.setChecked(true);

		} else {
			monologue.setVisibility(View.VISIBLE);
			jokeMonolbl.setVisibility(View.VISIBLE);
			jokeQlbl.setVisibility(View.GONE);
			jokeAnslbl.setVisibility(View.GONE);
			jokeQuestion.setVisibility(View.GONE);
			jokeAnswer.setVisibility(View.GONE);
			typeMono.setChecked(true);
		}

		typeGrp.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			/**
			 * Changes the visibility of certain components depending on what
			 * the user clicked.
			 */
			public void onCheckedChanged(RadioGroup rg, int checkedId) {
				if (typeQA.getId() == checkedId) {
					monologue.setVisibility(View.GONE);
					jokeMonolbl.setVisibility(View.GONE);
					jokeQlbl.setVisibility(View.VISIBLE);
					jokeAnslbl.setVisibility(View.VISIBLE);
					jokeQuestion.setVisibility(View.VISIBLE);
					jokeAnswer.setVisibility(View.VISIBLE);
					monologue.setText("");
				}

				else if (typeMono.getId() == checkedId) {
					monologue.setVisibility(View.VISIBLE);
					jokeMonolbl.setVisibility(View.VISIBLE);
					jokeQlbl.setVisibility(View.GONE);
					jokeAnslbl.setVisibility(View.GONE);
					jokeQuestion.setVisibility(View.GONE);
					jokeAnswer.setVisibility(View.GONE);
					jokeQuestion.setText("");
					jokeAnswer.setText("");
				}
			}
		});
	}

	/**
	 * Inflates the spinners components in the EditJoke activity.
	 */
	private void inflateSpinners() {
		categorySpinner = (Spinner) findViewById(R.id.category_spinner);
		ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter
				.createFromResource(this, R.array.category_array,
						android.R.layout.simple_spinner_item);
		categoryAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		categorySpinner.setAdapter(categoryAdapter);
		@SuppressWarnings("unchecked")
		ArrayAdapter<String> adapter = (ArrayAdapter<String>) categorySpinner
				.getAdapter();
		int pos = adapter.getPosition(jokeInfo.getString(jokeInfo
				.getColumnIndex("category")));
		categorySpinner.setSelection(pos);

		subCategorySpinner = (Spinner) findViewById(R.id.subCategory_spinner);

		sportsAdapter = ArrayAdapter.createFromResource(this,
				R.array.Sport_array, android.R.layout.simple_spinner_item);
		sportsAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		workAdapter = ArrayAdapter.createFromResource(this, R.array.Work_array,
				android.R.layout.simple_spinner_item);
		workAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		geekAdapter = ArrayAdapter.createFromResource(this, R.array.Geek_array,
				android.R.layout.simple_spinner_item);
		geekAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		categorySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				if (position == 0)
					subCategorySpinner.setAdapter(geekAdapter);

				else if (position == 1)
					subCategorySpinner.setAdapter(sportsAdapter);

				else
					subCategorySpinner.setAdapter(workAdapter);

				@SuppressWarnings("unchecked")
				ArrayAdapter<String> subCategoryAdapter = (ArrayAdapter<String>) subCategorySpinner
						.getAdapter();
				int pos = subCategoryAdapter.getPosition(jokeInfo
						.getString(jokeInfo.getColumnIndex("subCategory")));
				subCategorySpinner.setSelection(pos);
			}

			// do nothing method stub
			@Override
			public void onNothingSelected(AdapterView<?> parentView) { }
		});

		ratingSpinner = (Spinner) findViewById(R.id.rating_spinner);
		ArrayAdapter<CharSequence> ratingAdapter = ArrayAdapter
				.createFromResource(this, R.array.rating_array,
						android.R.layout.simple_spinner_item);
		ratingAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ratingSpinner.setAdapter(ratingAdapter);
		
		@SuppressWarnings("unchecked")
		ArrayAdapter<String> ratingStringAdapter = (ArrayAdapter<String>) ratingSpinner
				.getAdapter();
		pos = ratingStringAdapter.getPosition(jokeInfo.getString(jokeInfo
				.getColumnIndex("ratingScale")));
		ratingSpinner.setSelection(pos);

		booleanSpinner = (Spinner) findViewById(R.id.boolean_spinner);
		ArrayAdapter<CharSequence> booleanAdapter = ArrayAdapter
				.createFromResource(this, R.array.boolean_array,
						android.R.layout.simple_spinner_item);
		booleanAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		booleanSpinner.setAdapter(booleanAdapter);
		@SuppressWarnings("unchecked")
		ArrayAdapter<String> booleanSpinnerAdapter = (ArrayAdapter<String>) booleanSpinner
				.getAdapter();
		pos = booleanSpinnerAdapter.getPosition(jokeInfo.getString(jokeInfo
				.getColumnIndex("releaseStatus")));
		booleanSpinner.setSelection(pos);

	}

	/**
	 * Validates the user input.
	 * 
	 * @return if the input is valid or not
	 */
	private boolean validateInput() {
		boolean isValid = true;
		int size = txtFields.size();
		String field;

		// check which kind of joke was selected by the user
		if (typeQA.isChecked())
		{
			txtFields.get(3).setText("");
			
			for (int i = 0; i < size; i++) 
			{
				// only check the fields that relate to monologue-based questions
				if (i != 3)
				{
					field = txtFields.get(i).getText().toString();

					if (field.trim().length() == 0 || field.equals(""))
						isValid = false;
				}
			}
		}

		else
		{
			txtFields.get(1).setText("");
			txtFields.get(2).setText("");
			
			for (int i = 0; i < size; i++) 
			{
				// only check the fields that relate to monologue-based questions
				if (i != 1 && i != 2)
				{
					field = txtFields.get(i).getText().toString();

					if (field.length() == 0 || field.equals(""))
						isValid = false;
				}
			}
		}

		return isValid;
	}

	/**
	 * Event handler that updates the joke.
	 * 
	 * @param v
	 */
	public void onJokeUpdateClick(View v) {
		if (validateInput()) {
			jokeDBHelper.update(jokeInfo.getString(jokeInfo
					.getColumnIndex("_id")), categorySpinner.getSelectedItem()
					.toString(), subCategorySpinner.getSelectedItem()
					.toString(), Integer.toString(typeGrp
					.getCheckedRadioButtonId()), shortDescription.getText()
					.toString(), jokeQuestion.getText().toString(), jokeAnswer
					.getText().toString(), monologue.getText().toString(),
					ratingSpinner.getSelectedItem().toString(), comments
							.getText().toString(), source.getText().toString(),
					booleanSpinner.getSelectedItem().toString());
			editor.putString("newDesc", shortDescription.getText().toString());
			editor.commit();
			Toast.makeText(this, "Joke updated", Toast.LENGTH_LONG).show();
		} else
			Toast.makeText(this, "ERROR: fields cannot be blank",
					Toast.LENGTH_LONG).show();

	}

	/**
	 * Creates the options menu.
	 * 
	 * @param menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Determines the appropriate UI to launch depending on what was initiated
	 * by the user.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.settings:
			openPreferences();
			return true;
		case R.id.about:
			openAbout();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Opens the Preferences UI.
	 */
	public void openPreferences() {
		EditJoke.this
				.startActivity(new Intent(EditJoke.this, Preferences.class));
	}

	/**
	 * Opens the About UI.
	 */
	public void openAbout() {
		EditJoke.this.startActivity(new Intent(EditJoke.this, About.class));
	}

	@Override
	public void onPause() {
		super.onPause();
		if (validateInput()) {
			editor.putString("newDesc", shortDescription.getText().toString());
			System.out.println(shortDescription.getText().toString());
			editor.commit();
		}
	}
}