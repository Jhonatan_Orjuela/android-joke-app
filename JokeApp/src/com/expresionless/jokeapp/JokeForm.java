package com.expresionless.jokeapp;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Establishes the Joke form.
 * 
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class JokeForm extends Activity {
	private ArrayList<EditText> txtFields;

	private Spinner categorySpinner;
	private Spinner subCategorySpinner;
	private Spinner ratingSpinner;
	private Spinner booleanSpinner;
	private ArrayAdapter<CharSequence> geekAdapter;
	private ArrayAdapter<CharSequence> workAdapter;
	private ArrayAdapter<CharSequence> sportsAdapter;

	private RadioGroup typeGrp;
	private String typeCode;
	private RadioButton typeQA;
	private RadioButton typeMono;
	private EditText shortDescription;
	private EditText jokeQuestion;
	private EditText jokeAnswer;
	private EditText monologue;
	private EditText comments;
	private EditText source;

	private TextView jokeQlbl;
	private TextView jokeAnslbl;
	private TextView jokeMonolbl;
	private JokeDBHelper jokeDBHelper;
	private CategoryDBHelper categoryDBHelper;

	/**
	 * Creates the form and inflates all of the components.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.joke_form);
		jokeDBHelper = new JokeDBHelper(this);
		categoryDBHelper = new CategoryDBHelper(this);
		txtFields = new ArrayList<EditText>();
		inflateSpinners();
		inflateFormComponents();
	}

	/**
	 * Inflates the form components.
	 */
	private void inflateFormComponents() {
		shortDescription = (EditText) findViewById(R.id.shortDescEdit);
		txtFields.add(shortDescription);

		jokeQuestion = (EditText) findViewById(R.id.jokeQuestEdit);
		txtFields.add(jokeQuestion);
		jokeQlbl = (TextView) findViewById(R.id.jokeQuestLabel);

		jokeAnswer = (EditText) findViewById(R.id.jokeAnsEdit);
		txtFields.add(jokeAnswer);
		jokeAnslbl = (TextView) findViewById(R.id.jokeAnslbl);

		monologue = (EditText) findViewById(R.id.monologueEdit);
		txtFields.add(monologue);
		jokeMonolbl = (TextView) findViewById(R.id.monologueLbl);

		comments = (EditText) findViewById(R.id.commentsEdit);
		txtFields.add(comments);

		source = (EditText) findViewById(R.id.sourceEdit);
		txtFields.add(source);

		typeGrp = (RadioGroup) findViewById(R.id.radio_grp);
		typeCode = "1";

		typeQA = (RadioButton) findViewById(R.id.radio_QA);

		typeMono = (RadioButton) findViewById(R.id.radio_mono);
		monologue.setVisibility(View.GONE);
		jokeMonolbl.setVisibility(View.GONE);
		jokeQlbl.setVisibility(View.VISIBLE);
		jokeAnslbl.setVisibility(View.VISIBLE);
		jokeQuestion.setVisibility(View.VISIBLE);
		jokeAnswer.setVisibility(View.VISIBLE);
		typeGrp.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			/**
			 * Changes the visibility of certain components depending on what
			 * the user clicked.
			 */
			public void onCheckedChanged(RadioGroup rg, int checkedId) {
				if (typeQA.getId() == checkedId) {
					monologue.setVisibility(View.GONE);
					jokeMonolbl.setVisibility(View.GONE);
					jokeQlbl.setVisibility(View.VISIBLE);
					jokeAnslbl.setVisibility(View.VISIBLE);
					jokeQuestion.setVisibility(View.VISIBLE);
					jokeAnswer.setVisibility(View.VISIBLE);
					typeCode = "1";
				}

				else if (typeMono.getId() == checkedId) {
					monologue.setVisibility(View.VISIBLE);
					jokeMonolbl.setVisibility(View.VISIBLE);
					jokeQlbl.setVisibility(View.GONE);
					jokeAnslbl.setVisibility(View.GONE);
					jokeQuestion.setVisibility(View.GONE);
					jokeAnswer.setVisibility(View.GONE);
					typeCode = "2";
				}
			}
		});
	}

	/**
	 * Submits and adds the joke to both the database and ListView of jokes.
	 * 
	 * @param v
	 */
	public void submitClick(View v) {
		boolean isValid;
		isValid = validateInput();

		if (!isValid)
			Toast.makeText(getApplicationContext(),
					"ERROR. All text fields must be filled.", Toast.LENGTH_LONG)
					.show();
		else {
			Toast.makeText(getApplicationContext(), "Joke added.", Toast.LENGTH_LONG).show();
			
			System.out.println("Category Spinner:" + categorySpinner.getSelectedItem().toString());
			System.out.println("Subcategory Spinner:" + subCategorySpinner.getSelectedItem().toString());
			System.out.println("Typecode:" + typeCode);
			System.out.println("Short description:" + shortDescription.getText().toString());
			System.out.println("jokeQuestion:" + jokeQuestion.getText().toString());
			System.out.println("jokeAnswer:" + jokeAnswer.getText().toString());
			System.out.println("monologue:" + monologue.getText().toString());
			System.out.println("ratingSpinner:" + ratingSpinner.getSelectedItem().toString());
			System.out.println("comments:" + comments.getText().toString());
			System.out.println("source:" + source.getText().toString());
			System.out.println("booleanSpinner:" +booleanSpinner.getSelectedItem().toString());
			
			
			Cursor categoryCursor = categoryDBHelper.getCategories();
			categoryCursor.moveToFirst();
			
			while(!categoryCursor.getString(2).equals(subCategorySpinner.getSelectedItem().toString()))
				categoryCursor.moveToNext();
			
			int numberOfJokes = Integer.parseInt(categoryCursor.getString(3)) + 1;
			
			categoryDBHelper.update(categoryCursor.getString(0), categorySpinner.getSelectedItem().toString(), 
					subCategorySpinner.getSelectedItem().toString(), numberOfJokes);
				
			
			jokeDBHelper.insertNewJoke(categorySpinner.getSelectedItem().toString(), subCategorySpinner.getSelectedItem().toString(),
					typeCode, shortDescription.getText().toString(), jokeQuestion.getText().toString(), jokeAnswer.getText().toString(), monologue.getText().toString(), 
					ratingSpinner.getSelectedItem().toString(), comments.getText().toString(), source.getText().toString(), booleanSpinner.getSelectedItem().toString());
		}
	}

	/**
	 * Clears the form from any input that was previously entered.
	 * 
	 * @param v
	 */
	public void clearClick(View v) {
		shortDescription.setText("");
		jokeQuestion.setText("");
		jokeAnswer.setText("");
		monologue.setText("");
		comments.setText("");
		source.setText("");
	}

	/**
	 * Inflates each Spinner object.
	 */
	private void inflateSpinners() {
		categorySpinner = (Spinner) findViewById(R.id.category_spinner);
		ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter
				.createFromResource(this, R.array.category_array,
						android.R.layout.simple_spinner_item);
		categoryAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		categorySpinner.setAdapter(categoryAdapter);
		subCategorySpinner = (Spinner) findViewById(R.id.subCategory_spinner);

		sportsAdapter = ArrayAdapter.createFromResource(this,
				R.array.Sport_array, android.R.layout.simple_spinner_item);
		sportsAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		workAdapter = ArrayAdapter.createFromResource(this, R.array.Work_array,
				android.R.layout.simple_spinner_item);
		workAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		geekAdapter = ArrayAdapter.createFromResource(this, R.array.Geek_array,
				android.R.layout.simple_spinner_item);
		geekAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		categorySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				if (position == 0)
					subCategorySpinner.setAdapter(geekAdapter);

				else if (position == 1)
					subCategorySpinner.setAdapter(sportsAdapter);

				else
					subCategorySpinner.setAdapter(workAdapter);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});

		ratingSpinner = (Spinner) findViewById(R.id.rating_spinner);
		ArrayAdapter<CharSequence> ratingAdapter = ArrayAdapter
				.createFromResource(this, R.array.rating_array,
						android.R.layout.simple_spinner_item);
		ratingAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ratingSpinner.setAdapter(ratingAdapter);

		booleanSpinner = (Spinner) findViewById(R.id.boolean_spinner);
		ArrayAdapter<CharSequence> booleanAdapter = ArrayAdapter
				.createFromResource(this, R.array.boolean_array,
						android.R.layout.simple_spinner_item);
		booleanAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		booleanSpinner.setAdapter(booleanAdapter);

	}

	/**
	 * Validates the user input from what was entered in each field.
	 * 
	 * @return the boolean value denoting if the input was valid or not
	 */
	private boolean validateInput() {
		boolean isValid = true;
		int size = txtFields.size();
		String field;

		// check which kind of joke was selected by the user
		if (typeQA.isChecked())
		{
			txtFields.get(3).setText("");
			
			for (int i = 0; i < size; i++) 
			{
				// only check the fields that relate to monologue-based questions
				if (i != 3)
				{
					field = txtFields.get(i).getText().toString();

					if (field.trim().length() == 0 || field.equals(""))
						isValid = false;
				}
			}
		}

		else
		{
			txtFields.get(1).setText("");
			txtFields.get(2).setText("");
			
			for (int i = 0; i < size; i++) 
			{
				// only check the fields that relate to monologue-based questions
				if (i != 1 && i != 2)
				{
					field = txtFields.get(i).getText().toString();

					if (field.length() == 0 || field.equals(""))
						isValid = false;
				}
			}
		}

		return isValid;
	}

	/**
	 * Creates the options menu.
	 * 
	 * @param menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Determines the appropriate UI to launch depending on what was initiated
	 * by the user.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.settings:
			openPreferences();
			return true;
		case R.id.about:
			openAbout();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Opens the Preferences UI.
	 */
	public void openPreferences() {
		JokeForm.this
				.startActivity(new Intent(JokeForm.this, Preferences.class));
	}

	/**
	 * Opens the About UI.
	 */
	public void openAbout() {
		JokeForm.this.startActivity(new Intent(JokeForm.this, About.class));
	}
}