package com.expresionless.jokeapp;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity used to show the joke.
 * 
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class ShowJoke extends Activity {
	private static final String PREFS_NAME = "PrefsFile";
	private final String HOST = "http://waldo2.dawsoncollege.qc.ca/1011681/php/";
	private String joke;
	private SharedPreferences prefs;
	private SharedPreferences.Editor editor;
	private JokeDBHelper jokeDBHelper;
	private Cursor jokeInfo;
	private TextView textMonologue;
	private TextView textAnswer;
	private CategoryDBHelper categoryDBHelper;
	private final String TAG = "ShowJoke";
	private ImageView imageView;
	private String url;
	private ProgressBar bar;
	
	/**
	 * Creates the activity and sets the layout.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.show_joke);
		categoryDBHelper = new CategoryDBHelper(this);

		prefs = getSharedPreferences(PREFS_NAME, 0);

		joke = prefs.getString("Joke", "nothing");

		jokeDBHelper = new JokeDBHelper(this);

		jokeInfo = jokeDBHelper.getJokeByShortDescription(joke);
		jokeInfo.moveToFirst();

		imageView = (ImageView) findViewById(R.id.localImage);
		TextView textJoke = (TextView) findViewById(R.id.joke);
		textMonologue = (TextView) findViewById(R.id.monologue);
		Button answer = (Button) findViewById(R.id.showAnswer);
		Button showJoke = (Button) findViewById(R.id.showNextLine);
		textAnswer = (TextView) findViewById(R.id.answer);
		bar = (ProgressBar) findViewById(R.id.progressBar3);
		bar.setVisibility(ProgressBar.INVISIBLE);
		System.out.println(jokeInfo.getString(jokeInfo.getColumnIndex("answer")));
		System.out.println(jokeInfo.getString(jokeInfo.getColumnIndex("question")));
		System.out.println(jokeInfo.getString(jokeInfo.getColumnIndex("monologue")));

		System.out.print("Answer: " + jokeInfo.getString(jokeInfo.getColumnIndex("answer")));
		
		if (!jokeInfo.getString(jokeInfo.getColumnIndex("answer")).equals("null"))
		{
			if (!jokeInfo.getString(jokeInfo.getColumnIndex("answer")).isEmpty() || 
					!jokeInfo.getString(jokeInfo.getColumnIndex("answer")).equals("")) 
			{
				joke = jokeInfo.getString(jokeInfo.getColumnIndex("question"));
				textJoke.setText(joke);
				showJoke.setVisibility(View.GONE);
				textMonologue.setVisibility(View.GONE);
			}

			else if (jokeInfo.getString(jokeInfo.getColumnIndex("question")).equals("") ||
					jokeInfo.getString(jokeInfo.getColumnIndex("question")).equals("null")) 
			{
				joke = jokeInfo.getString(jokeInfo.getColumnIndex("monologue"));
				String jokeP1 = joke.substring(0, joke.indexOf("*"));
				joke = joke.substring(joke.indexOf("*") + 1);
				answer.setVisibility(View.GONE);
				textMonologue.setText(jokeP1);
				textJoke.setVisibility(View.GONE);
				textAnswer.setVisibility(View.GONE);
			}
		}
		
		else
		{
			url = HOST + jokeInfo.getString(jokeInfo.getColumnIndex("question"));
			
			textJoke.setVisibility(View.GONE);
			showJoke.setVisibility(View.GONE);
			textMonologue.setVisibility(View.GONE);
			answer.setVisibility(View.GONE);
			textAnswer.setVisibility(View.GONE);
			
			new GetImage().execute(url);
		}
	}

	/**
	 * Event handler used to show the answer.
	 * 
	 * @param v
	 */
	public void onShowAnswerClick(View v) {
		textAnswer.setText(jokeInfo.getString(jokeInfo.getColumnIndex("answer")));
	}

	/**
	 * Displays the next line of the joke.
	 * 
	 * @param v
	 */
	public void onNextLineClick(View v) {
		if (joke.length() != 0) 
		{
			if (joke.indexOf("*") != -1) 
			{
				String jokeP1 = joke.substring(0, joke.indexOf("*"));
				textMonologue.setText(textMonologue.getText() + jokeP1);
				joke = joke.substring(joke.indexOf("*") + 1);
			} 
			else
			{
				textMonologue.setText(textMonologue.getText() + joke);
				joke = "";
			}
		}
	}

	/**
	 * Event listener that allows the user to edit a joke.
	 * 
	 * @param v
	 */
	public void onEditClick(View v) {
		ShowJoke.this.startActivity(new Intent(ShowJoke.this, EditJoke.class));
		ShowJoke.this.finish();
	}

	/**
	 * Event listener that allows the user to delete a joke.
	 * 
	 * @param v
	 */
	public void onDeleteClick(View v) {
		AlertDialog.Builder deleteAlert = new AlertDialog.Builder(this);
		deleteAlert.setMessage("Are you sure you want to delete?");
		deleteAlert.setCancelable(true);

		// if the user confirmed - delete the joke
		deleteAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int id) {
				jokeDBHelper.deleteJoke(jokeInfo.getInt(jokeInfo
						.getColumnIndex("_id")));

				editor = prefs.edit();
				editor.putBoolean("deleted", true);
				editor.commit();

				Cursor categoryCursor = categoryDBHelper.getCategories();
				categoryCursor.moveToFirst();

				while(!categoryCursor.getString(2).equals(jokeInfo.getString(2)))
					categoryCursor.moveToNext();

				int numberOfJokes = Integer.parseInt(categoryCursor.getString(3)) - 1;

				categoryDBHelper.update(categoryCursor.getString(0), categoryCursor.getString(1), 
						categoryCursor.getString(2), numberOfJokes);
			}
		});

		// otherwise - cancel the dialog
		deleteAlert.setNegativeButton("No", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		AlertDialog alertD = deleteAlert.create();
		alertD.show();
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}

	/**
	 * Creates the options menu.
	 * 
	 * @param menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Determines the appropriate UI to launch depending on what was initiated
	 * by the user.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) 
		{
			case R.id.settings:
				openPreferences();
				return true;
			case R.id.about:
				openAbout();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Opens the Preferences UI.
	 */
	public void openPreferences() {
		ShowJoke.this
				.startActivity(new Intent(ShowJoke.this, Preferences.class));
	}

	/**
	 * Opens the About UI.
	 */
	public void openAbout() {
		ShowJoke.this.startActivity(new Intent(ShowJoke.this, About.class));
	}
	
	/**
	 * AsyncTask inner class used to get the image.
	 * 
	 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
	 */
	private class GetImage extends AsyncTask<String, Void, Bitmap> {
		HttpURLConnection httpConnection = null;

		@Override
		protected void onPreExecute() {
			bar.setVisibility(ProgressBar.VISIBLE);
		}

		/**
		 * Background task.
		 */
		@Override
		protected Bitmap doInBackground(String... urls) {
			Bitmap map = downloadImage(url);
			publishProgress();
			// return -> onPostExecute(map)
			return map;
		}

		// on UI thread after background task
		@Override
		protected void onPostExecute(Bitmap map) {
			bar.setVisibility(ProgressBar.INVISIBLE);
			if (map == null)
				Toast.makeText(getApplicationContext(),
						"Unable to read bitmap", Toast.LENGTH_LONG).show();
			else
				imageView.setImageBitmap(map);
			if (httpConnection != null)
				httpConnection.disconnect();
		}


		/**
		 * Updates the progress bar.
		 */
		@Override
		protected void onProgressUpdate(Void...progress) {
			// setting progress percentage
			bar.setProgress(0);
		}

		/**
		 * Create the HTTPURLConnection.
		 * 
		 * @param urlString
		 * @return
		 * @throws IOException
		 */
		private InputStream getHttpConnection(String urlString)
				throws IOException {
			InputStream stream = null;

			// return null if the network is not ready
			if (!isNetReady())
				return null;

			URL url = new URL(urlString);
			URLConnection connection = url.openConnection();

			try {
				httpConnection = (HttpURLConnection) connection;
				httpConnection.connect();

				if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
					stream = httpConnection.getInputStream();
			}

			catch (Exception e) {
				Log.d(TAG, getClass().getSimpleName()
						+ " getHttpConnection() Exception:" + e.getMessage());

				e.printStackTrace();
			}

			return stream;
		}

		/**
		 * Creates bitmap image from the InputStream.
		 * 
		 * @param url		the URL being passed in for the image.
		 * @return			the image.
		 */
		private Bitmap downloadImage(String url) {
			Bitmap bitmap = null;
			InputStream stream = null;

			BitmapFactory.Options bmOptions = new BitmapFactory.Options();
			// use image original size, not smaller image
			bmOptions.inSampleSize = 1;

			try {
				stream = getHttpConnection(url);

				if (stream != null) {
					bitmap = BitmapFactory.decodeStream(stream, null, bmOptions);
					stream.close();
				}
			}

			catch (IOException e) {
				Log.d(TAG, getClass().getSimpleName()
						+ "  downloadImage() Exception:" + e.getMessage());
				e.printStackTrace();
			}

			return bitmap;
		}
	}

	/**
	 * Checks if the network is connected.
	 * 
	 * @return	boolean value that infers if the connection is established.
	 */
	private boolean isNetReady() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		
		if (networkInfo != null && networkInfo.isConnected())
			return true;
		
	    else 
	    {
			Toast.makeText(this, "Error: Connection not available", Toast.LENGTH_LONG).show();
			return false;
		}
	}
}