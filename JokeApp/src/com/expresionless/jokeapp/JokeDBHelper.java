package com.expresionless.jokeapp;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

/**
 * Database helper used to construct the Joke table. Performs other operations
 * such as inserting and deleting.
 * 
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class JokeDBHelper {
	private final static String DB_NAME = "joke.db";
	private final static int DB_VERSION = 1;
	private final static String TABLE_JOKE = "Joke";
	private DatabaseOpenHelper dbOpenHelper;
	@SuppressWarnings("unused")
	private Context context;
	private String path = "data/data/com.expresionless.jokeapp/databases/";
	private File dbFile;

	// columns for Joke table
	private final static String COL_ID = "_id";
	private final static String COL_CATEGORY = "category";
	private final static String COL_SUBCATEGORY = "subCategory";
	private final static String COL_JOKE_TYPE_CODE = "typeCode";
	private final static String COL_JOKE_SHORT_DESCRIPTION = "shortDescription";
	private final static String COL_QUESTION_TEXT = "question";
	private final static String COL_ANSWER_TEXT = "answer";
	private final static String COL_MONOLOGUE_TEXT = "monologue";
	private final static String COL_RATING_SCALE = "ratingScale";
	private final static String COL_COMMENTS = "comments";
	private final static String COL_JOKE_SOURCE = "jokeSource";
	private final static String COL_RELEASE_STATUS = "releaseStatus";
	private final static String COL_CREATE_DATE = "createDate";
	private final static String COL_MODIFY_DATE = "modifyDate";

	/**
	 * Constructor that receives the context.
	 * 
	 * @param context
	 */
	public JokeDBHelper(Context context) {
		this.dbOpenHelper = new DatabaseOpenHelper(context, DB_NAME, null,
				DB_VERSION);
		this.context = context;
		populateDatabase();
	}

	/**
	 * @return the colJokeShortDescription
	 */
	public String getColJokeShortDescription() {
		return COL_JOKE_SHORT_DESCRIPTION;
	}

	/**
	 * Returns the monologue column content.
	 * 
	 * @return the colMonologueText
	 */
	public String getColMonologueText() {
		return COL_MONOLOGUE_TEXT;
	}

	/**
	 * Returns the question text column content.
	 * 
	 * @return the colQuestionText
	 */
	public String getColQuestionText() {
		return COL_QUESTION_TEXT;
	}

	// ///////////////////// CRUD METHODS ///////////////////////

	/**
	 * Deletes a joke from the database.
	 * 
	 * @param id
	 *            the id of the joke to be deleted
	 */
	public void deleteJoke(int id) {
		dbOpenHelper.getWritableDatabase().delete(TABLE_JOKE, COL_ID + "=?",
				new String[] { String.valueOf(id) });
	}

	/**
	 * Queries every record in the Joke table.
	 * 
	 * @return the cursor containing every record
	 */
	public Cursor getJokes() {
		return dbOpenHelper.getWritableDatabase().query(TABLE_JOKE, null, null,
				null, null, null, null);
	}

	/**
	 * Queries the database and extracts the jokes depending on the category
	 * 
	 * @param cat
	 *            - the category
	 * @return - the jokes in that category
	 */
	public Cursor getJokeByCategory(String[] cat) {
		if (cat[0] != null)
			if (cat[1] != null)
				if (cat[2] != null)
					return dbOpenHelper.getWritableDatabase().query(
							TABLE_JOKE,
							null,
							COL_CATEGORY + "=?" + " OR " + COL_CATEGORY + "=?"
									+ " OR " + COL_CATEGORY + "=?", cat, null,
							null, null);
				else
					return dbOpenHelper.getWritableDatabase().query(TABLE_JOKE,
							null,
							COL_CATEGORY + "=?" + " OR " + COL_CATEGORY + "=?",
							new String[] { cat[0], cat[1] }, null, null, null);

			else
				return dbOpenHelper.getWritableDatabase().query(TABLE_JOKE,
						null, COL_CATEGORY + "=?", new String[] { cat[0] },
						null, null, null);
		else
			return dbOpenHelper.getWritableDatabase().query(TABLE_JOKE, null,
					null, null, null, null, null);
	}

	/**
	 * Returns the short description of a particular joke.
	 * 
	 * @param description
	 * @return
	 */
	public Cursor getJokeByShortDescription(String description) {
		return dbOpenHelper.getWritableDatabase().query(TABLE_JOKE, null,
				COL_JOKE_SHORT_DESCRIPTION + "=?",
				new String[] { description }, null, null, null);
	}

	/**
	 * Inserts a new joke as a record in the database.
	 * 
	 * @param category
	 * @param subCategory
	 * @param typeCode
	 * @param shortDescription
	 * @param question
	 * @param answer
	 * @param monologue
	 * @param ratingScale
	 * @param comments
	 * @param jokeSource
	 * @param releaseStatus
	 * @param createDate
	 * @param modifyDate
	 * @return code
	 */
	public long insertNewJoke(String category, String subCategory,
			String typeCode, String shortDescription, String question,
			String answer, String monologue, String ratingScale,
			String comments, String jokeSource, String releaseStatus) {
		Locale currentLocale = new Locale("en", "CA");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd",
				currentLocale);
		GregorianCalendar date = new GregorianCalendar();

		ContentValues cv = new ContentValues();

		cv.put(COL_CATEGORY, category);
		cv.put(COL_SUBCATEGORY, subCategory);
		cv.put(COL_JOKE_TYPE_CODE, typeCode);
		cv.put(COL_JOKE_SHORT_DESCRIPTION, shortDescription);
		cv.put(COL_QUESTION_TEXT, question);
		cv.put(COL_ANSWER_TEXT, answer);
		cv.put(COL_MONOLOGUE_TEXT, monologue);
		cv.put(COL_RATING_SCALE, ratingScale);
		cv.put(COL_COMMENTS, comments);
		cv.put(COL_JOKE_SOURCE, jokeSource);
		cv.put(COL_RELEASE_STATUS, releaseStatus);
		cv.put(COL_MODIFY_DATE, dateFormat.format(date.getTime()));

		long code = dbOpenHelper.getWritableDatabase().insert(TABLE_JOKE, null,
				cv);

		return code;
	}

	public long update(String id, String category, String subCategory,
			String typeCode, String shortDescription, String question,
			String answer, String monologue, String ratingScale,
			String comments, String jokeSource, String releaseStatus) {
		Locale currentLocale = new Locale("en", "CA");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd",
				currentLocale);
		GregorianCalendar date = new GregorianCalendar();

		ContentValues cv = new ContentValues();

		cv.put(COL_CATEGORY, category);
		cv.put(COL_SUBCATEGORY, subCategory);
		cv.put(COL_JOKE_TYPE_CODE, typeCode);
		cv.put(COL_JOKE_SHORT_DESCRIPTION, shortDescription);
		cv.put(COL_QUESTION_TEXT, question);
		cv.put(COL_ANSWER_TEXT, answer);
		cv.put(COL_MONOLOGUE_TEXT, monologue);
		cv.put(COL_RATING_SCALE, ratingScale);
		cv.put(COL_COMMENTS, comments);
		cv.put(COL_JOKE_SOURCE, jokeSource);
		cv.put(COL_RELEASE_STATUS, releaseStatus);
		cv.put(COL_MODIFY_DATE, dateFormat.format(date.getTime()));

		long code = dbOpenHelper.getWritableDatabase().update(TABLE_JOKE, cv,
				COL_ID + "= ?", new String[] { id });

		return code;
	}

	/**
	 * Populates the database with the jokes.
	 */
	private void populateDatabase() {

		dbFile = new File(path + DB_NAME);
		if (!dbFile.exists()) {
			String[] monologueJokes = {
					"A neutron walked into a bar and asked how much a drink was;* the bartender replied, 'For you, no charge.'*",
					"Biology is the only science in which multiplication is the same thing as division.*",
					"There are 10 types of people:* those who understand binary,* and those who do not understand it.*",
					"It is easier to change the specification to fit the program than vice versa.*",	
					"A good golf partner is one who's always a little bit worse than you are.*",
					"Golf is what you play when you're too out of shape to play softball.*",
					"The Toronto Maple Leafs have won the Stanley Cup.*",
					"Internships are synonymous for free labour.*",
					"If you had a dollar for every time you felt like telling off your boss *- you would be rich!*" };

			// Science Jokes
			insertNewJoke("Geek", "Science", "2", "Neutrons in a Bar", "", "",
					monologueJokes[0], "10", "joke about charges",
					"www.jupiterscientific.org/sciinfo/sciencejokes.html",
					"true");

			insertNewJoke("Geek", "Science", "2", "Biology joke", "", "",
					monologueJokes[1], "10", "Such laugh", "source", "true");

			insertNewJoke("Geek", "Computer Science", "2", "Binary joke", "",
					"", monologueJokes[2], "10", "so funny ahaha", "source",
					"true");

			insertNewJoke("Geek", "Computer Science", "2",
					"Computer Science specs.", "", "", monologueJokes[3], "10",
					"so many lulz", "source", "true");
			
			insertNewJoke("Geek", "Computer Science", "1",
					"Computer Science Doctor", "What kind of doctor fixes broken websites?", 
					"An URLologist", "", "10",
					"so many lulz", "source", "true");

			// Sports Jokes
			insertNewJoke("Sports", "Golf", "1", "Golf joke",
					"How many golfers does it take to change a light bulb?",
					"FORE!", "", "10", "golf ball", "source", "true");

			insertNewJoke("Sports", "Golf", "2", "Golf partner joke", "", "",
					monologueJokes[4], "10", "Comment goes here.", "source",
					"true");
			
			insertNewJoke("Sports", "Golf", "2", "Golf reality", "", "",
					monologueJokes[5], "10", "Comment goes here.", "source",
					"true");

			insertNewJoke("Sports", "Hockey", "2", "Hockey trash talk", "", "",
					monologueJokes[6], "10", "Comment goes here.", "source",
					"true");

			insertNewJoke("Sports", "Hockey", "1", "Hockey trash talk #2",
					"What's the difference between a fight in boxing "
							+ " versus hockey?",
					"The ones in hockey are real.", "", "10",
					"Comment goes here.", "source", "true");

			// Work Jokes
			insertNewJoke("Work", "Secretary", "1", "Funny secretary joke",
					"The one quality you search for in a secretary?",
					" How fast he or she can get you a coffee.", "", "10",
					"I told you it was funny", "source", "true");

			insertNewJoke("Work", "Secretary", "1", "Secretary phone joke",
					"Don't feel like answering the phone?",
					"Ask your secretary.", "", "10",
					"it's funny... really. it is", "source", "true");

			insertNewJoke(
					"Work",
					"Intern",
					"1",
					"Intern Exploitation",
					"Have tedious work you don't feel like doing?",
					"Hand it over to the intern - they'll be more than willing.",
					"", "10", "Comment goes here.", "source", "true");

			insertNewJoke("Work", "Intern", "2", "Intern real talk", "", "",
					monologueJokes[7], "10", "#somethingkanyewouldsay",
					"source", "true");

			insertNewJoke("Work", "Boss", "2", "Boss getting on your nerves?",
					"", "", monologueJokes[8], "10",
					"These jokes are SO funny", "source", "true");

			insertNewJoke("Work", "Boss", "1", "Your Boss and stairs",
					"What do your boss and a slinky have in common?",
					"They're both " + "fun to watch tumble down the stairs.",
					"", "10", "BEST JOKE APP EVVAAA!", "source", "true");
			insertNewJoke("Geek", "Computer Science", "2",
					"It's a joke... get it?", "", 
					"", "This app is amazing!* Just kidding!* It's a piece of crap*", "10",
					"so many lulz", "sauce pls", "true");
		}
	}

	/**
	 * Inner class containing the Database life-cycle methods.
	 * 
	 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
	 */
	private class DatabaseOpenHelper extends SQLiteOpenHelper 
	{
		public DatabaseOpenHelper(Context context, String name,
				CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		/**
		 * Creates the Joke table.
		 */
		@Override
		public void onCreate(SQLiteDatabase db) {
			// consider changing some of this (data types aren't clear)
			String createTableSQL = "CREATE TABLE " + TABLE_JOKE + "(" + COL_ID
					+ " integer primary key autoincrement, " + COL_CATEGORY
					+ " varchar(25) not null, " + COL_SUBCATEGORY
					+ " varchar(25) not null, " + COL_JOKE_TYPE_CODE
					+ " integer(1) not null, " + COL_JOKE_SHORT_DESCRIPTION
					+ " varchar(140) not null, " + COL_QUESTION_TEXT
					+ " varchar(140) not null, " + COL_ANSWER_TEXT
					+ " varchar(140) not null, " + COL_MONOLOGUE_TEXT
					+ " varchar(300) not null, " + COL_RATING_SCALE
					+ " integer2 not null, " + COL_COMMENTS
					+ " varchar(140) not null, " + COL_JOKE_SOURCE
					+ " varchar(400) not null, " + COL_RELEASE_STATUS
					+ " boolean not null, " + COL_CREATE_DATE
					+ " timestamp default current_timestamp, "
					+ COL_MODIFY_DATE + " date not null " + ");";

			db.execSQL(createTableSQL);
		}

		/**
		 * Changes the defined version to a later version.
		 * 
		 * @param db
		 * @param oldVersion
		 * @param newVersion
		 */
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(JokeDBHelper.class.getName(),
					"Upgrading database from version " + oldVersion + " to "
							+ newVersion + ", which will destroy all old data.");

			db.execSQL("DROP TABLE IF EXISTS " + TABLE_JOKE);

			onCreate(db);
		}
	}
}