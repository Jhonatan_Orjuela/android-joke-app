package com.expresionless.jokeapp;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity used to show the joke from the web.
 * 
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class ShowWebJoke extends Activity {

	private TextView webQuestion;
	private TextView webAnswer;
	private TextView webMonologue;
	private Button showWebAnswer;
	private Button showWebNextLine;
	private ProgressBar bar;
	
	private final String HOST = "http://waldo2.dawsoncollege.qc.ca/1011681/php/";
	private String jokeId;
	private final String TAG = "expressionless";
	private ImageView imageView;
	private String url;
	private String[] jokeInfo;
	 private int progress;
	private JokeDBHelper jokeDBHelper;
	private CategoryDBHelper categoryDBHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_web_joke);
		jokeId = getIntent().getExtras().getBundle("idBundle").getString("id");
		imageView = (ImageView) findViewById(R.id.downloadedImage);
		bar = (ProgressBar) findViewById(R.id.progressBar2);
		bar.setVisibility(ProgressBar.INVISIBLE);
		inflateComponents();
		
		new GetWebData().execute(jokeId);		
	}
	
	/**
	 * Inserts the necessary data into the jokeInfo array.
	 */
	private void insertData() {
		if(jokeInfo[6] != "null") {
			webQuestion.setVisibility(View.VISIBLE);
			showWebAnswer.setVisibility(View.VISIBLE);
			webQuestion.setText(jokeInfo[5]);	
		}
		
		else if(jokeInfo[7] != "null") {
			webMonologue = (TextView) findViewById(R.id.web_monologue);
			webMonologue.setVisibility(View.VISIBLE);
			
			showWebNextLine = (Button) findViewById(R.id.show_web_next_line);
			showWebNextLine.setVisibility(View.VISIBLE);
		}
		
		else {
			url = HOST + jokeInfo[5];
			new GetImage().execute(url);
		}
	}
	
	/**
	 * Event handler that takes care of displaying a next line.
	 * 
	 * @param v
	 */
	public void onNextLineClick(View v) 
	{
		if (jokeInfo[7].length() != 0)
		{
			// check if there is a delimeter present
			if (jokeInfo[7].indexOf("*") != -1)
			{
				String jokeP1 = jokeInfo[7].substring(0, jokeInfo[7].indexOf("*"));
				webMonologue.setText(webMonologue.getText() + jokeP1);
				jokeInfo[7] = jokeInfo[7].substring(jokeInfo[7].indexOf("*") + 1);
			} 
			
			else 
			{
				webMonologue.setText(webMonologue.getText() + jokeInfo[7]);
				jokeInfo[7] = "";
			}
		}
	}
	
	/**
	 * Event handler that shows the answer.
	 * 
	 * @param v
	 */
	public void onShowAnswerClick(View v) {
		webAnswer.setVisibility(View.VISIBLE);
		webAnswer.setText(jokeInfo[6]);
	}
	
	/**
	 * Event handler that deals with adding a web-based joke.
	 * 
	 * @param v
	 */
	public void onWebAddClick(View v) 
	{	
		System.out.println(jokeInfo[1] + jokeInfo[2] + jokeInfo[3] + jokeInfo[4] + jokeInfo[5] + jokeInfo[6] + jokeInfo[7]);
		jokeDBHelper.insertNewJoke(jokeInfo[1], jokeInfo[2], jokeInfo[3],
				jokeInfo[4], jokeInfo[5], jokeInfo[6], jokeInfo[7],
				jokeInfo[8], jokeInfo[9], jokeInfo[10], jokeInfo[11]);
		
		Cursor categoryCursor = categoryDBHelper.getCategories();
		categoryCursor.moveToFirst();
		
		while(!categoryCursor.getString(2).equals(jokeInfo[2])) {
			System.out.println(categoryCursor.getString(2));
			System.out.println(jokeInfo[2]);
			categoryCursor.moveToNext();
		}
		
		int numberOfJokes = Integer.parseInt(categoryCursor.getString(3)) + 1;
		
		categoryDBHelper.update(categoryCursor.getString(0), jokeInfo[1], 
				jokeInfo[2], numberOfJokes);
		
		Toast.makeText(this, "Joke Added to Local Database.", Toast.LENGTH_LONG).show();
	}
	
	/**
	 * Inflates all needed components for this activity.
	 */
	private void inflateComponents() {
		webQuestion = (TextView) findViewById(R.id.web_joke);
		webQuestion.setVisibility(View.GONE);
		
		webAnswer = (TextView) findViewById(R.id.web_answer);
		webAnswer.setVisibility(View.GONE);
		
		webMonologue = (TextView) findViewById(R.id.web_monologue);
		webMonologue.setVisibility(View.GONE);
		
		showWebAnswer = (Button) findViewById(R.id.show_web_answer);
		showWebAnswer.setVisibility(View.GONE);
		
		showWebNextLine = (Button) findViewById(R.id.show_web_next_line);
		showWebNextLine.setVisibility(View.GONE);
		
		jokeDBHelper = new JokeDBHelper(this);
		categoryDBHelper = new CategoryDBHelper(this);
	}
	
	
	/**
	 * Check if the network is connected.
	 * 
	 * @return
	 */
	private boolean isNetReady()
	{
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		
		if (networkInfo != null && networkInfo.isConnected())
			return true;
		
		else 
		{
			Toast.makeText(this, "Error: Connection not available", Toast.LENGTH_LONG).show();
			return false;
		}
	}
	
	/**
     * Locates the data based on the select query sent in from the PHP located
     * on the server.
     *
     * @param matchKey
     * @return
     * @throws MalformedURLException
     * @throws IOException
     */
	public String findData(String matchKey) throws MalformedURLException, IOException 
	{
		String newFeed = "http://waldo2.dawsoncollege.qc.ca/1011681/php/searchJokeById.php?id="
				+ matchKey;

		StringBuilder response = new StringBuilder();
		URL url = new URL(newFeed);

		HttpURLConnection httpconn = (HttpURLConnection) url.openConnection();
		httpconn.connect();

		// check if the HTTPConnection is okay
		if (httpconn.getResponseCode() == HttpURLConnection.HTTP_OK) {
			InputStream in = new BufferedInputStream(httpconn.getInputStream());

			BufferedReader input = new BufferedReader(new InputStreamReader(in));
			String strLine = null;
			
			// loop while there is a new line
			while ((strLine = input.readLine()) != null)
				response.append(strLine);

			if (input != null)
				input.close();

			if (httpconn != null)
				httpconn.disconnect();
		}

		System.out.println(response.toString());

		return response.toString();
	}
	
	/**
     * Inner class used to get the data from the web - such as the JSON
     *
     * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
     */
	private class GetWebData extends AsyncTask<String, Integer, String>
	{
		/**
		 * Background task.
		 */
		protected String doInBackground(String... matchKey) {

			Log.d(TAG, " id:" + matchKey[0]);
			try {
				// send the data to onPostExecute()
				return findData(matchKey[0]);
			} catch (Exception e) {
				Log.d(TAG, "doInBackground() Exception:" + e.getMessage());
				e.printStackTrace();
				return "";
			}
		}
		
		/**
	     * Processes the received JSON response and analyzes the values contained
	     * within it.
	     *
	     * @param resp
	     * @throws IllegalStateException
	     * @throws IOException
	     * @throws JSONException
	     * @throws NoSuchAlgorithmException
	     */
		public void processJSONResponse(String resp) throws IllegalStateException,
				IOException, JSONException, NoSuchAlgorithmException {
			
			JSONObject jobj = new JSONObject(resp);
			jokeInfo = new String[14];
			
			// check if response is not null
			if (!(resp == null)) 
			{
				// named items in array
				if (jobj.has("_id"))
				{
					Log.d(TAG, "id: " + jobj.getString("_id"));

					// Collection attached to the ListView
					jokeInfo[0] = jobj.getString("_id");
				}
				
				// check if each field is present in the JSON project
				if (jobj.has("category"))
					jokeInfo[1] = jobj.getString("category");
				
				if (jobj.has("subCategory"))
					jokeInfo[2] = jobj.getString("subCategory");
				
				if (jobj.has("typeCode"))
					jokeInfo[3] = jobj.getString("typeCode");
				
				if (jobj.has("shortDescription"))
					jokeInfo[4] = jobj.getString("shortDescription");
				
				if (jobj.has("question"))
					jokeInfo[5] = jobj.getString("question");
				
				if (jobj.has("answer"))
					jokeInfo[6] = jobj.getString("answer");
				
				if (jobj.has("monologue")) 
					jokeInfo[7] = jobj.getString("monologue");
				
				if (jobj.has("rating")) 
					jokeInfo[8] = jobj.getString("rating");
				
				if (jobj.has("comments"))
					jokeInfo[9] = jobj.getString("comments");
				
				if (jobj.has("source"))
					jokeInfo[10] = jobj.getString("source");
				
				if (jobj.has("releaseStatus"))
					jokeInfo[11] = jobj.getString("releaseStatus");
				
				if (jobj.has("createDate"))
					jokeInfo[12] = jobj.getString("createDate");
				
				if (jobj.has("modifyDate"))
					jokeInfo[13] = jobj.getString("modifyDate");

				Log.d(TAG, " id:" + jokeInfo[0] + "Category: " + jokeInfo[1]
						+ "SubCategory: " + jokeInfo[2] + "Typecode: "
						+ jokeInfo[3] + "ShortDescription: " + jokeInfo[4]
								+ "Question: " + jokeInfo[5]);

				insertData();
			}
		}
		
		/**
         * Takes care of processing the JSON response and fires the WebResults
         * activity.
         */
		protected void onPostExecute(String result) {
			try {
				// data from the HTTP connection stream is in JSON format
				processJSONResponse(result);				
				
			} catch (Exception e) {
				Log.d(TAG, "onPostExecute() Exception:" + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	/**
	* Inner class that acquires the image from the web.
     *
     * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
	 */
	private class GetImage extends AsyncTask<String, Void, Bitmap>
	{
		HttpURLConnection httpConnection = null;

		@Override
		protected void onPreExecute() {
			bar.setVisibility(ProgressBar.VISIBLE);
			progress = 0;
		}

		/**
		 * Background task.
		 */
		@Override
		protected Bitmap doInBackground(String... urls) {
			Bitmap map = downloadImage(url);
			publishProgress();

			// return -> onPostExecute(map)
			return map;
		}

		/**
		 * Updating the progress bar.
		 */
		@Override
		protected void onProgressUpdate(Void...voids) {
			// setting progress percentage
			bar.setProgress(progress);
		}
		
		// on UI thread after background task
		@Override
		protected void onPostExecute(Bitmap map) {
			bar.setVisibility(ProgressBar.INVISIBLE);
			if (map == null)
				Toast.makeText(getApplicationContext(),
						"Unable to read bitmap", Toast.LENGTH_LONG).show();
			else
				imageView.setImageBitmap(map);

			if (httpConnection != null)
				httpConnection.disconnect();
		}

		/**
		 * Create the HTTPURLConnection.
		 * 
		 * @param urlString
		 * @return
		 * @throws IOException
		 */
		private InputStream getHttpConnection(String urlString)
				throws IOException {
			InputStream stream = null;

			if (!isNetReady())
				return null;

			URL url = new URL(urlString);
			URLConnection connection = url.openConnection();

			try {
				httpConnection = (HttpURLConnection) connection;
				httpConnection.connect();

				if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
					stream = httpConnection.getInputStream();
			}

			catch (Exception e) {
				Log.d(TAG, getClass().getSimpleName()
						+ " getHttpConnection() Exception:" + e.getMessage());

				e.printStackTrace();
			}

			return stream;
		}

		/**
		 * Creates Bitmap image from the Input stream.
		 * 
		 * @param url		the URL being passed in for the image.
		 * @return
		 */
		private Bitmap downloadImage(String url) {
			Bitmap bitmap = null;
			InputStream stream = null;

			BitmapFactory.Options bmOptions = new BitmapFactory.Options();
			bmOptions.inSampleSize = 1;

			try {
				stream = getHttpConnection(url);

				if (stream != null) {
					bitmap = BitmapFactory.decodeStream(stream, null, bmOptions);
					stream.close();
				}
			}

			catch (IOException e) {
				Log.d(TAG, getClass().getSimpleName()
						+ "  downloadImage() Exception:" + e.getMessage());
				e.printStackTrace();
			}

			return bitmap;
		}
	}
}