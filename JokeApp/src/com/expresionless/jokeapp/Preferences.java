package com.expresionless.jokeapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

/**
 * Preferences activity used to change the content view and start the
 * appropriate activity.
 * 
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class Preferences extends Activity {
	private SharedPreferences prefs;
	private SharedPreferences.Editor editor;
	private static final String PREFS_NAME = "PrefsFile";

	private ToggleButton tglBtnSplashScreen;
	private ToggleButton tglBtnGeekCategory;
	private ToggleButton tglBtnSportsCategory;
	private ToggleButton tglBtnWorkCategory;
	private ToggleButton tglBtnEllipsize;
	private ToggleButton tglBtnStartup;

	/**
	 * Creates the activity and sets the layout.
	 */
	@SuppressLint("CommitPrefEdits")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.preferences);
		inflateComponents();
		
		prefs = getSharedPreferences(PREFS_NAME, 0);
		editor = prefs.edit();
		setToggleButtons();
	}
	
	/**
	 * Changes the state of the settings toggle buttons depending on
	 * the preferences.
	 */
	private void setToggleButtons()
	{
		if (prefs.getBoolean("splashScreen", true))
			tglBtnSplashScreen.setChecked(true);
		
		else
			tglBtnSplashScreen.setChecked(false);
		
		if (prefs.getBoolean("geekCategory", true))
			tglBtnGeekCategory.setChecked(true);
		
		else
			tglBtnGeekCategory.setChecked(false);
		
		if (prefs.getBoolean("sportsCategory", true))
			tglBtnSportsCategory.setChecked(true);
		
		else
			tglBtnSportsCategory.setChecked(false);
		
		if (prefs.getBoolean("workCategory", true))
			tglBtnWorkCategory.setChecked(true);
		
		else
			tglBtnWorkCategory.setChecked(false);
		
		if (!prefs.getBoolean("startUp", false))
			tglBtnStartup.setChecked(false);
		
		else
			tglBtnStartup.setChecked(true);
	}
	

	@Override
	public void onPause() {
		super.onPause();
		editor.putBoolean("splashScreen", tglBtnSplashScreen.isChecked());
		editor.putBoolean("geekCategory", tglBtnGeekCategory.isChecked());
		editor.putBoolean("sportsCategory", tglBtnSportsCategory.isChecked());
		editor.putBoolean("workCategory", tglBtnWorkCategory.isChecked());
		editor.putBoolean("ellipsize", tglBtnEllipsize.isChecked());
		editor.putBoolean("startUp", tglBtnStartup.isChecked());
		editor.commit();
	}

	/**
	 * Updates the shared preferences.
	 * 
	 * @param v
	 */
	public void onUpdateClick(View v) {
		editor.putBoolean("splashScreen", tglBtnSplashScreen.isChecked());
		editor.putBoolean("geekCategory", tglBtnGeekCategory.isChecked());
		editor.putBoolean("sportsCategory", tglBtnSportsCategory.isChecked());
		editor.putBoolean("workCategory", tglBtnWorkCategory.isChecked());
		editor.putBoolean("ellipsize", tglBtnEllipsize.isChecked());
		editor.putBoolean("startUp", tglBtnStartup.isChecked());
		
		Toast.makeText(this, "Updated", Toast.LENGTH_LONG).show();
		editor.commit();
	}

	/**
	 * Inflates the GUI components.
	 */
	private void inflateComponents() {
		tglBtnSplashScreen = (ToggleButton) findViewById(R.id.toggleBtnSplashScreen);
		tglBtnWorkCategory = (ToggleButton) findViewById(R.id.toggleBtnWorkCategory);
		tglBtnGeekCategory = (ToggleButton) findViewById(R.id.toggleBtnGeekCategory);
		tglBtnSportsCategory = (ToggleButton) findViewById(R.id.toggleBtnSportsCategory);
		tglBtnEllipsize = (ToggleButton) findViewById(R.id.toggleBtnEllipsize);
		tglBtnStartup = (ToggleButton) findViewById(R.id.toggleBtnStartup);
	}

	/**
	 * Creates the options menu.
	 * 
	 * @param menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Determines the appropriate UI to launch depending on what was initiated
	 * by the user.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.settings:
			openPreferences();
			return true;
		case R.id.about:
			openAbout();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Opens the Preferences UI.
	 */
	public void openPreferences() {
		Preferences.this.startActivity(new Intent(Preferences.this,
				Preferences.class));
	}

	/**
	 * Opens the About UI.
	 */
	public void openAbout() {
		Preferences.this
				.startActivity(new Intent(Preferences.this, About.class));
	}
}