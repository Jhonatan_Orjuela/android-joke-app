package com.g3w14.action;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.g3w14.data.ClientBean;
import com.g3w14.persistence.ClientDAOImp;


/**
 * This is the action bean that takes care of the account.xhtml
 * This is where the user can change shipping info, password, and email.
 * 
 * @author Sean-Frankel Gaon Canlas
 *
 */
@Named("accountBean")
@SessionScoped
public class AccountBean implements Serializable {

	@Inject
	private ClientBean clientBean;
	@Inject
	private ClientDAOImp clientDAO;
	
	private String email;
	private String password;
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * This is the set method for the clientBean
	 * @param clientBean
	 */
	public String setClientBean(ClientBean clientBean) {
		this.clientBean = clientBean;
		return "toAccount";
	}
	
	/**
	 * This is the get method for the clientBean
	 * @return
	 */
	public ClientBean getClientBean() {
		return clientBean;
	}
	
	/**
	 * This is the method that is called when the user wishes to change their email.
	 * @throws SQLException 
	 */
	public String changeEmail() throws SQLException {
		
		clientDAO.updateClientEmail(clientBean);
		
		return "toAccount";
	}
	
	/**
	 * This is the method that is called when the user wishes to change their password.
	 * @throws SQLException 
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchAlgorithmException 
	 */
	public String changePassword() throws NoSuchAlgorithmException, InvalidKeySpecException, SQLException {

		clientDAO.updateClientPassword(clientBean);
		
		return "toAccount";
	}

	/**
	 * This is the getter for email.
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * This is the setter for email.
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
		clientBean.setEmail(email);
	}

	/**
	 * This is the getter for password.
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * This is the setter for email.
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
		clientBean.setPassword(password);
	}
	
}
