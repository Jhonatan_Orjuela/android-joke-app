/**
 * 
 */
package com.g3w14.action;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.g3w14.data.RSSBean;
import com.g3w14.persistence.RSSDAOImp;

/**
 * this is the action beans to manage the logic for the RSS feed
 * 
 * @author 1040134 Jhonatan Orjuela
 * 
 */
@Named("rssManager")
@SessionScoped
public class RSSActionBean implements Serializable {

	@Inject
	private RSSDAOImp rssDao;
	
	private RSSBean rssBean;
	
	private String rssUrl;
	

	/**
	 * 
	 */
	public RSSActionBean() {
		super();
	}
	
	/**
	 * 
	 * @param rssUrl the rssUrl to set
	 */
	public void setRssUrl(String rssUrl) {
		this.rssUrl = rssUrl;
	}
	/**
	 * set the current feed to display
	 * @param id
	 * @throws SQLException
	 */
	public void setRSSFeedToDisplay(int id) throws SQLException {
	
		rssBean = new RSSBean();
		rssBean.setId(id);
		rssBean.setDisplay(true);
		rssDao.changeRSSDisplaysToFalse();
		rssDao.updateRSS(rssBean);
		
}
	/**
	 * inserts feed into our database
	 * @throws SQLException
	 */
	public void insertNewFeed() throws SQLException{
		rssBean = new RSSBean();
		rssBean.setRssUrl(rssUrl);
		rssDao.insertRSS(rssBean);
	}
	

}
