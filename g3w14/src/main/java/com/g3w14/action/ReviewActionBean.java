/**
 * 
 */
package com.g3w14.action;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.g3w14.data.ClientBean;
import com.g3w14.data.CustomerReviewBean;
import com.g3w14.persistence.CustomerReviewDAOImp;

/**
 * @author Jhonatan Orjuela
 *
 */
@Named("bookReviewAction")
@SessionScoped
public class ReviewActionBean implements Serializable {

	

private CustomerReviewBean review;

private ArrayList<CustomerReviewBean> bookReviews;

private ArrayList<CustomerReviewBean> singleBookReview;
@Inject
private CustomerReviewDAOImp rdao;

@Inject
private LoginBean client;


private String reviewString;

private int reviewRating;

private int clientId;

private String theISBN;

/**
 * @return the theISBN
 */
public String getTheISBN() {
	return theISBN;
}

/**
 * @param theISBN the theISBN to set
 */
public void setTheISBN(String theISBN) {
	System.out.println("the isbn gets set to" + theISBN);
	this.theISBN = theISBN;
}

/**
 * @return the cliendId
 */
public int getClientId() {
	return clientId;
}

/**
 * @param cliendId the cliendId to set
 */
public void setClientId(int cliendId) {
	System.out.println("the id gets set to" + cliendId);
	this.clientId = cliendId;
}

private String reviewTitle;

/**
 * @return the reviewTitle
 */
public String getReviewTitle() {
	return reviewTitle;
}

/**
 * @param reviewTitle the reviewTitle to set
 */
public void setReviewTitle(String reviewTitle) {
	this.reviewTitle = reviewTitle;
}

/**
 * @return the reviewString
 */
public String getReviewString() {
	return reviewString;
}

/**
 * @param reviewString the reviewString to set
 */
public void setReviewString(String reviewString) {
	this.reviewString = reviewString;
}

private static final long serialVersionUID = 1L;


public ReviewActionBean(){
	super();
}

public ArrayList<CustomerReviewBean> getReviewsByISBN(String ISBN) throws SQLException{
	review = new CustomerReviewBean();
	
	review.setIsbn(ISBN);
	
	bookReviews = rdao.getSpecificQueryRecords(review);
	return bookReviews;
}

public ArrayList<CustomerReviewBean> getSingleReviewByISBN(String anIsbn)throws SQLException{
	singleBookReview = new ArrayList<>();
	review= new CustomerReviewBean();
	review.setIsbn(anIsbn);
	
	singleBookReview.add(rdao.getSingleReviewbyISBN(review));
	
	return singleBookReview;
	
}
public void setRatingStar(String rating){
	reviewRating = Integer.parseInt(rating);
	System.out.println("this is the rating " +rating);
}

public boolean displayStarOne(int theRating){
	if (theRating >= 1)
		return true;
	else
		return false;
}

public boolean displayStarTwo(int theRating){
	
	if (theRating >= 2)
		return true;
	else
		return false;
}
public boolean displayStarThree(int theRating){
	if (theRating >= 3)
		return true;
	else
		return false;
}
public boolean displayStarFour(int theRating){
	if (theRating >= 4)
		return true;
	else
		return false;
}
public boolean displayStarFive(int theRating){
	
	if (theRating == 5)
		return true;
	else
		return false;
}
public void registerReview(String anIsbn)throws SQLException{
	review = new CustomerReviewBean();
	System.out.println("this is the client id :" + client.getClientBean().getId());
	System.out.println("this is the rating : " + reviewRating);
	System.out.println("this is the title : " +reviewTitle);
	System.out.println("this is the review text : " + reviewString);
	System.out.println("thisnis the ISBN :" + anIsbn);
	int theid= client.getClientBean().getId();
	review.setClientId(theid);
	review.setRating(reviewRating);
	review.setName(reviewTitle);
	review.setText(reviewString);
	review.setIsbn(anIsbn);
	
	
	rdao.insertRecord(review);
	
}

}
