package com.g3w14.action;

import java.io.Serializable;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.g3w14.data.SurveyBean;
import com.g3w14.persistence.SurveyDAOImp;

/**
 * This is the survey action bean.
 * @author Sean-Frankel Gaon Canlas, Theo De Villeneuve
 *
 */
@Named("surveyActionBean")
@SessionScoped
public class SurveyActionBean implements Serializable{

	@Inject
	private SurveyDAOImp sdao;
	@Inject
	private SurveyBean survey;
	private boolean voted = false;
	private ArrayList<SurveyBean> surveyList;
	private String result;
	
	private static final long serialVersionUID = 1L;
	
	public SurveyActionBean() {
		super();
	}
	
	/**
	 * Setter method for survey list. This method gets all the surveys in the database.
	 * @throws SQLException
	 */
	private void setSurveyList() throws SQLException {
		surveyList = sdao.getSurveyRecords();
	}
	
	/**
	 * Getter method for survey.
	 * @return
	 * @throws SQLException
	 */
	public SurveyBean getSurvey() throws SQLException {
		setSurveyList();
		setSurvey();
		return survey;
	}
	
	/**
	 * This is the method that formats the results into percent.
	 * @param quantity
	 * @return
	 */
	public String getPercent(int quantity){
		
		float percent;
		float c = new Float(quantity);
		percent = c / survey.getTotalVotes();
		percent = percent * 100;
		
		DecimalFormat df = new DecimalFormat("##.##");
		
		return df.format(percent);
	}
	
	/**
	 * This is the method that sets the specific method that isn't disabled.
	 */
	public void setSurvey() {
		for(int i = 0; i < surveyList.size(); i++)
			if(!surveyList.get(i).getDisable())
				survey = surveyList.get(i);
	}
	
	/**
	 * This is the method that adds the person's votes to the database.
	 * @param vote
	 * @return
	 * @throws SQLException
	 */
	public String addVotes(String vote) throws SQLException {
		voted = true;
		
		if(vote.equals("1"))
			survey.setVotes1(survey.getVotes1() + 1);
		else if(vote.equals("2"))
			survey.setVotes2(survey.getVotes2() + 1);
		else if(vote.equals("3"))
			survey.setVotes3(survey.getVotes3() + 1);
		else 
			survey.setVotes4(survey.getVotes4() + 1);
		
		int total = survey.getVotes1() + survey.getVotes2() + survey.getVotes3() + survey.getVotes4();
		
		survey.setTotalVotes(total);
		
		sdao.updateSurvey(survey);
		
		return "";

	}

	/**
	 * Getter method for result.
	 * @return
	 */
	public String getResult() {
		return result;
	}

	/**
	 * Setter method for result.
	 * @param result
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * This is the method to check if the person has voted yet or not.
	 * @return the voted
	 */
	public boolean isVoted() {
		return voted;
	}

	/**
	 * Setter method for voted.
	 * @param voted the voted to set
	 */
	public void setVoted(boolean voted) {
		this.voted = voted;
	}

}
