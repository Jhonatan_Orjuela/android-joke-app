package com.g3w14.action;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.g3w14.data.BookBean;
import com.g3w14.data.ClientBean;
import com.g3w14.persistence.BookDAOImp;

/**
 * This action Bean class takes care of extracting groups of books or single
 * books from the database in order to display them.
 * 
 * @author Tyler Patricio, Jhonatan Orjuela , Sophie Leduc Major, Sean-Frankel
 *         Gaon Canlas
 */
@Named("displayBooksBean")
@SessionScoped
public class DisplayBooksBean implements Serializable {
	@Inject
	private BookDAOImp bdao;
	@Inject
	private BookBean singleBook;

	private static final long serialVersionUID = 1L;
	private ArrayList<BookBean> topSellers;
	private ArrayList<BookBean> searchResults;
	private ArrayList<BookBean> allBooks;
	private ArrayList<BookBean> booksOnSale;
	private String queryString;

	// Specifies the type of search to be done
	private String searchType = "3";
	// The search string sent from the user
	private String searchCredentials;

	/**
	 * Constructor for DisplayBooksBean
	 */
	public DisplayBooksBean() {
		super();
	}

	/**
	 * @return the searchResults
	 */
	public ArrayList<BookBean> getSearchResults() {
		System.out.println("Search results size: " + searchResults.size());
		return searchResults;
	}

	/**
	 * @return the searchCredentials
	 */
	public String getSearchCredentials() {
		return searchCredentials;
	}

	/**
	 * @param searchCredentials
	 *            the searchCredentials to set
	 */
	public void setSearchCredentials(String searchCredentials) {
		System.out.println("these are the search credentials: "
				+ searchCredentials);
		this.searchCredentials = searchCredentials;
	}

	public String setMenuItem(String menuGenre) {
		setSearchCredentials(menuGenre);
		return "toResult";
	}

	/**
	 * @return the searchType
	 */
	public String getSearchType() {
		return searchType;
	}

	/**
	 * @param searchType
	 *            the searchType to set
	 */
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	/**
	 * @return the queryString
	 */
	public String getQueryString() {
		return queryString;
	}

	/**
	 * @param queryString
	 *            the queryString to set
	 */
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	/**
	 * This method sets the books for banner.
	 * 
	 * @throws SQLException
	 */
	public void setTopSellers() throws SQLException {
		topSellers = bdao.getTopSellers();
	}

	/**
	 * This method retrieves the top selling books so that they can be displayed
	 * in the banner.
	 * 
	 * @return a list of books
	 * @throws SQLException
	 */
	public ArrayList<BookBean> getTopSellers() throws SQLException {
		setTopSellers();
		return topSellers;
	}

	/**
	 * This methods sets the ArrayList for allBooks which contains all the books
	 * in the database. (Testing purposes)
	 * 
	 * @throws SQLException
	 */
	public void setAllBooks() throws SQLException {
		allBooks = bdao.getAllRecords();
	}

	/**
	 * This method returns the allBooks array which contains all the books.
	 * 
	 * @return the allBooks ArrayList that has all the books.
	 * @throws SQLException
	 */
	public ArrayList<BookBean> getAllBooks() throws SQLException {
		setAllBooks();
		return allBooks;
	}

	/**
	 * This method gets all the books on sale.
	 * @return the booksOnSale
	 * @throws SQLException
	 */
	public ArrayList<BookBean> getBooksOnSale() throws SQLException {
		booksOnSale = bdao.getBooksOnSale();
		return booksOnSale;
	}

	/**
	 * @param booksOnSale
	 *            the booksOnSale to set
	 */
	public void setBooksOnSale(ArrayList<BookBean> booksOnSale) {
		this.booksOnSale = booksOnSale;
	}

	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<BookBean> searchBooksEngine() throws SQLException {

		System.out.println("searchType: " + searchType);

		if (searchType.equals("1"))
			getBooksByTitle();
		else if (searchType.equals("2"))
			getBooksByAuthor();
		else if (searchType.equals("3"))
			getBooksByGenre();

		return null;
	}

	/**
	 * This method is used to retrieve the books by genre so they can be
	 * displayed.
	 * 
	 * @param genre
	 *            the genre specified
	 * @return booksByGenre the books of a specific genre
	 * @throws SQLException
	 */
	public void getBooksByGenre() throws SQLException {
		// if (searchCredentials == null || searchCredentials.isEmpty())
		// throw new
		// IllegalArgumentException("ERROR. Invalid genre parameter.");

		BookBean bb = new BookBean();
		bb.setGenre(searchCredentials);

		// BookDAOImp bdao = new BookDAOImp();
		System.out.println("In list for genre: \n" + bdao.advancedSearch(bb));
		System.out.println("date entered: " + bb.getDateEntered());

		searchResults = bdao.advancedSearch(bb);

	}

	/**
	 * 
	 * @throws SQLException
	 */
	public void getBooksByAuthor() throws SQLException {
		// if (searchCredentials == null || searchCredentials.isEmpty())
		// throw new
		// IllegalArgumentException("ERROR. Invalid genre parameter.");

		BookBean bb = new BookBean();
		bb.setAuthor("%" + searchCredentials + "%");

		// BookDAOImp bdao = new BookDAOImp();
		System.out.println("In list for author: \n" + bdao.advancedSearch(bb));
		System.out.println("date entered: " + bb.getDateEntered());

		searchResults = bdao.advancedSearch(bb);
	}

	/**
	 * Retrieves the books that match with the search string
	 * 
	 * @throws SQLException
	 */
	public void getBooksByTitle() throws SQLException {

		// if (searchCredentials == null || searchCredentials.isEmpty())
		// throw new
		// IllegalArgumentException("ERROR. Invalid genre parameter.");

		BookBean bb = new BookBean();
		bb.setTitle("%" + searchCredentials + "%");

		// BookDAOImp bdao = new BookDAOImp();
		System.out.println("In list for title: \n" + bdao.advancedSearch(bb));
		System.out.println("date entered: " + bb.getDateEntered());

		searchResults = bdao.advancedSearch(bb);
	}

	/**
	 * This method is used to retrieve a single book so it can be displayed.
	 * 
	 * @return the book corresponding to the name
	 * @throws SQLException
	 */
	public BookBean getSingleBook() throws SQLException {
		return singleBook;
	}

	/**
	 * This method sets the singleBook based on the ISBN13 gotten from the
	 * query.
	 * 
	 * @throws SQLException
	 * @throws IOException
	 */
	public String setSingleBook() throws SQLException, IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext()
				.getRequestParameterMap();
		String isbn13 = params.get("isbn13");

		if (isbn13 == null || isbn13.isEmpty() || isbn13.equals("")) {
			fc.getExternalContext().responseSendError(404, "");
			fc.responseComplete();

			return null;
		} else {
			BookBean bb = new BookBean();
			bb.setIsbn13(isbn13);
			singleBook = bdao.getQuerySingleBook(bb.getIsbn13());
			return "toBookView";
		}

	}

	/**
	 * This method is used when we get a recurring user and retrieving the
	 * previously looked books
	 * 
	 * @param bookcookies
	 *            the cookies saved from a returning user
	 * @return the previously looked at books
	 */
	public BookBean retrievePreviousBookInterests(ClientBean c) {
		BookBean bb = new BookBean();
		bb.setIsbn13(c.getLastSearch());

		return bb;
	}

	/**
	 * changes the numerical value of the book type to a corresponding string
	 * 
	 * @param numberType
	 * @return
	 */
	public String changeNumberTypeToString(int numberType) {

		if (numberType == 1)
			return "Paperback";
		else if (numberType == 2)
			return "HardCover";
		else
			return "eBook";
	}

	/*
	 * private void setUpResults(ArrayList<BookBean> bb){
	 * 
	 * int parcels = bb.size() / 5; int ctr = 0; for(int i =0; i < parcels;
	 * i++){ ArrayList<BookBean> bean = new ArrayList<BookBean>(); for(int j =
	 * 0; j < 5; j++){ bean.add(bb.get(ctr)); } searchResults.add(bean); }
	 * 
	 * 
	 * }
	 */

	/**
	 * This is an overloaded method for getbooksByGenre. This is a slightly
	 * different one because the way the other was made requires a searchType to
	 * be set as well when I just needed a genre. This is mainly used in the
	 * BookView to show books of the same genre.
	 * 
	 * @param genre
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<BookBean> getBooksByGenre(String genre)
			throws SQLException {

		System.out.println("I get here abd the genre is: " + genre);

		BookBean bb = new BookBean();
		bb.setGenre(genre);
		ArrayList<BookBean> books = bdao.advancedSearch(bb);

		for (int i = 0; i < books.size(); i++) {
			if (books.get(i).getIsbn13().equals(singleBook.getIsbn13()))
				books.remove(i);
		}

		return books;
	}
}