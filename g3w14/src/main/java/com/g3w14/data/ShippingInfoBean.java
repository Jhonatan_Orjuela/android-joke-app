package com.g3w14.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * @author Jhonatan Orjuela 1040134, Sophie Leduc Major 0931442
 * 
 *
 */

@Named("shippingInfoBean")
@SessionScoped
public class ShippingInfoBean implements Serializable {

	private static final long serialVersionUID = 8740022670799314589L;
	
	private int id;
	private int clientid;
	private String shipName;
	private String shipTitle;
	private String shipLastName;
	private String shipFirstName;
	private String shipCompany;
	private String shipAddress1;
	private String shipAddress2;
	private String shipCity;
	private String shipProvince;
	private String shipCountry;
	private String shipPostalCode;
	
	
	
	public ShippingInfoBean() {
		super();
	}



	public ShippingInfoBean(int id, int clientid, String shipName,
			String shipTitle, String shipLastName, String shipFirstName,
			String shipCompany, String shipAddress1, String shipAddress2,
			String shipCity, String shipProvince, String shipCountry,
			String shipPostalCode) {
		super();
		this.id = id;
		this.clientid = clientid;
		this.shipName = shipName;
		this.shipTitle = shipTitle;
		this.shipLastName = shipLastName;
		this.shipFirstName = shipFirstName;
		this.shipCompany = shipCompany;
		this.shipAddress1 = shipAddress1;
		this.shipAddress2 = shipAddress2;
		this.shipCity = shipCity;
		this.shipProvince = shipProvince;
		this.shipCountry = shipCountry;
		this.shipPostalCode = shipPostalCode;
	}
	
	private static Map<String,Object> shiptitlevalue;
	static{
		shiptitlevalue = new LinkedHashMap<String,Object>();
		shiptitlevalue.put("Mr.", "Mr"); //label, value
		shiptitlevalue.put("Mrs.", "Mrs");
		shiptitlevalue.put("Ms.", "Ms");
	}
 
	public Map<String,Object> getShipTitleValue() {
		return shiptitlevalue;
	}
	
	private static Map<String,Object> shipprovincevalue;
	static{
		shipprovincevalue = new LinkedHashMap<String,Object>();
		shipprovincevalue.put("Alberta", "AB"); //label, value
		shipprovincevalue.put("British Columbia", "BC");
		shipprovincevalue.put("Manitoba", "MB");
		shipprovincevalue.put("Brunswick", "NB");
		shipprovincevalue.put("Newfoundland and Labrador", "NL");
		shipprovincevalue.put("Nova Scotia", "NS");
		shipprovincevalue.put("Ontario", "ON");
		shipprovincevalue.put("Prince Edward Island", "PE");
		shipprovincevalue.put("Quebec", "QC");
		shipprovincevalue.put("Saskatchewan", "SK");
		shipprovincevalue.put("Northwest Territories", "NT");
		shipprovincevalue.put("Nunavut", "NU");
		shipprovincevalue.put("Yukon", "YT");
	}
 
	public Map<String,Object> getShipProvinceValue() {
		return shipprovincevalue;
	}
	
	private static Map<String,Object> shipcountryvalue;
	static{
		shipcountryvalue = new LinkedHashMap<String,Object>();
		shipcountryvalue.put("Canada", "Canada"); //label, value
		shipcountryvalue.put("United States of America", "USA");
	}
 
	public Map<String,Object> getShipCountryValue() {
		return shipcountryvalue;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getClientid() {
		return clientid;
	}



	public void setClientid(int clientid) {
		this.clientid = clientid;
	}



	public String getShipName() {
		return shipName;
	}



	public void setShipName(String shipName) {
		this.shipName = shipName;
	}



	public String getShipTitle() {
		return shipTitle;
	}



	public void setShipTitle(String shipTitle) {
		this.shipTitle = shipTitle;
	}



	public String getShipLastName() {
		return shipLastName;
	}



	public void setShipLastName(String shipLastName) {
		this.shipLastName = shipLastName;
	}



	public String getShipFirstName() {
		return shipFirstName;
	}



	public void setShipFirstName(String shipFirstName) {
		this.shipFirstName = shipFirstName;
	}



	public String getShipCompany() {
		return shipCompany;
	}



	public void setShipCompany(String shipCompany) {
		this.shipCompany = shipCompany;
	}



	public String getShipAddress1() {
		return shipAddress1;
	}



	public void setShipAddress1(String shipAddress1) {
		this.shipAddress1 = shipAddress1;
	}



	public String getShipAddress2() {
		return shipAddress2;
	}



	public void setShipAddress2(String shipAddress2) {
		this.shipAddress2 = shipAddress2;
	}



	public String getShipCity() {
		return shipCity;
	}



	public void setShipCity(String shipCity) {
		this.shipCity = shipCity;
	}



	public String getShipProvince() {
		return shipProvince;
	}



	public void setShipProvince(String shipProvince) {
		this.shipProvince = shipProvince;
	}



	public String getShipCountry() {
		return shipCountry;
	}



	public void setShipCountry(String shipCountry) {
		this.shipCountry = shipCountry;
	}



	public String getShipPostalCode() {
		return shipPostalCode;
	}



	public void setShipPostalCode(String shipPostalCode) {
		this.shipPostalCode = shipPostalCode;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + clientid;
		result = prime * result + id;
		result = prime * result
				+ ((shipAddress1 == null) ? 0 : shipAddress1.hashCode());
		result = prime * result
				+ ((shipAddress2 == null) ? 0 : shipAddress2.hashCode());
		result = prime * result
				+ ((shipCity == null) ? 0 : shipCity.hashCode());
		result = prime * result
				+ ((shipCompany == null) ? 0 : shipCompany.hashCode());
		result = prime * result
				+ ((shipCountry == null) ? 0 : shipCountry.hashCode());
		result = prime * result
				+ ((shipFirstName == null) ? 0 : shipFirstName.hashCode());
		result = prime * result
				+ ((shipLastName == null) ? 0 : shipLastName.hashCode());
		result = prime * result
				+ ((shipName == null) ? 0 : shipName.hashCode());
		result = prime * result
				+ ((shipPostalCode == null) ? 0 : shipPostalCode.hashCode());
		result = prime * result
				+ ((shipProvince == null) ? 0 : shipProvince.hashCode());
		result = prime * result
				+ ((shipTitle == null) ? 0 : shipTitle.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShippingInfoBean other = (ShippingInfoBean) obj;
		if (clientid != other.clientid)
			return false;
		if (id != other.id)
			return false;
		if (shipAddress1 == null) {
			if (other.shipAddress1 != null)
				return false;
		} else if (!shipAddress1.equals(other.shipAddress1))
			return false;
		if (shipAddress2 == null) {
			if (other.shipAddress2 != null)
				return false;
		} else if (!shipAddress2.equals(other.shipAddress2))
			return false;
		if (shipCity == null) {
			if (other.shipCity != null)
				return false;
		} else if (!shipCity.equals(other.shipCity))
			return false;
		if (shipCompany == null) {
			if (other.shipCompany != null)
				return false;
		} else if (!shipCompany.equals(other.shipCompany))
			return false;
		if (shipCountry == null) {
			if (other.shipCountry != null)
				return false;
		} else if (!shipCountry.equals(other.shipCountry))
			return false;
		if (shipFirstName == null) {
			if (other.shipFirstName != null)
				return false;
		} else if (!shipFirstName.equals(other.shipFirstName))
			return false;
		if (shipLastName == null) {
			if (other.shipLastName != null)
				return false;
		} else if (!shipLastName.equals(other.shipLastName))
			return false;
		if (shipName == null) {
			if (other.shipName != null)
				return false;
		} else if (!shipName.equals(other.shipName))
			return false;
		if (shipPostalCode == null) {
			if (other.shipPostalCode != null)
				return false;
		} else if (!shipPostalCode.equals(other.shipPostalCode))
			return false;
		if (shipProvince == null) {
			if (other.shipProvince != null)
				return false;
		} else if (!shipProvince.equals(other.shipProvince))
			return false;
		if (shipTitle == null) {
			if (other.shipTitle != null)
				return false;
		} else if (!shipTitle.equals(other.shipTitle))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "ShippingInfoBean [id=" + id + ", clientid=" + clientid
				+ ", shipName=" + shipName + ", shipTitle=" + shipTitle
				+ ", shipLastName=" + shipLastName + ", shipFirstName="
				+ shipFirstName + ", shipCompany=" + shipCompany
				+ ", shipAddress1=" + shipAddress1 + ", shipAddress2="
				+ shipAddress2 + ", shipCity=" + shipCity + ", shipProvince="
				+ shipProvince + ", shipCountry=" + shipCountry
				+ ", shipPostalCode=" + shipPostalCode + "]";
	}
	
	
	
	
}
