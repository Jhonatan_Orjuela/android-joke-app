package com.g3w14.data;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Bean class that denotes all of the variables relating to the 
 * advertisements table.
 * 
 * @author Tyler Patricio
 */
@Named("advertisementBean")
@RequestScoped
public class AdvertisementBean implements Serializable
{
	private int adId;
	private String adFileName;
	private String url;
	private boolean display;
		
	/**
	 * No-parameter constructor. 
	 */
	public AdvertisementBean()
	{
		this.adId = 0;
		this.adFileName = "";
		this.url = "";
		this.display = false;
	}

	/**
	 * Parameter-filled constructor.
	 * 
	 * @param adId
	 * @param adFileName
	 * @param url
	 */
	public AdvertisementBean(int adId, String adFileName, String url, boolean display)
	{
		this.adId = adId;
		this.adFileName = adFileName;
		this.url = url;
		this.display = display;
	}

	/**
	 * @return the adId
	 */
	public int getAdId()
	{
		return adId;
	}

	/**
	 * @param adId the adId to set
	 */
	public void setAdId(int adId)
	{
		this.adId = adId;
	}

	/**
	 * @return the adFileName
	 */
	public String getAdFileName()
	{
		return adFileName;
	}

	/**
	 * @param adFileName the adFileName to set
	 */
	public void setAdFileName(String adFileName)
	{
		this.adFileName = adFileName;
	}

	/**
	 * @return the url
	 */
	public String getUrl()
	{
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url)
	{
		this.url = url;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AdvertisementBean [adId=" + adId + ", adFileName=" + adFileName
				+ ", url=" + url + ", display=" + display + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((adFileName == null) ? 0 : adFileName.hashCode());
		result = prime * result + adId;
		result = prime * result + (display ? 1231 : 1237);
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AdvertisementBean other = (AdvertisementBean) obj;
		if (adFileName == null) {
			if (other.adFileName != null)
				return false;
		} else if (!adFileName.equals(other.adFileName))
			return false;
		if (adId != other.adId)
			return false;
		if (display != other.display)
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	/**
	 * @return the display
	 */
	public boolean isDisplay() {
		return display;
	}

	/**
	 * @param display the display to set
	 */
	public void setDisplay(boolean display) {
		this.display = display;
	}
}