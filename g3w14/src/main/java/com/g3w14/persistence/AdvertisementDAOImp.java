package com.g3w14.persistence;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;

import com.g3w14.data.AdvertisementBean;

/**
 * This is the implementation class of the AdvertisementDAO interface. It is the
 * DAO for the Advertisements table.
 * 
 * @author Sean-Frankel Gaon Canlas
 * @author Sophie Leduc Major
 * 
 */
@Named("advertisementAction")
@RequestScoped
public class AdvertisementDAOImp implements AdvertisementDAO, Serializable 
{
	@Resource(name = "jdbc/g3w14")
	private DataSource dataSource;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.g3w14.persistence.AdvertisementDAO#getQueryRecords()
	 */
	@Override
	public ArrayList<AdvertisementBean> getQueryRecords() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		ArrayList<AdvertisementBean> abList = new ArrayList<>();
		String preparedSQL = "SELECT * FROM advertisements";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL);
				ResultSet resultSet = ps.executeQuery()) {
			while (resultSet.next()) {
				AdvertisementBean ab = new AdvertisementBean();
				ab.setAdId(resultSet.getInt("adid"));
				ab.setAdFileName(resultSet.getString("adFileName"));
				ab.setUrl(resultSet.getString("url"));
				ab.setDisplay(resultSet.getBoolean("display"));
				abList.add(ab);
			}
		}
		return abList;
	}

	@Override
	public ArrayList<AdvertisementBean> getSpecificQueryRecords(String adFileName)
			throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		ArrayList<AdvertisementBean> abList = new ArrayList<>();
		String preparedSQL = "SELECT * FROM advertisements where adfilename =?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection
						.prepareStatement(preparedSQL);) {
			pStatement.setString(1, adFileName);

			try (ResultSet resultSet = pStatement.executeQuery();) {
				while (resultSet.next()) {
					
					AdvertisementBean ab = new AdvertisementBean();
					
					ab.setAdId(resultSet.getInt("adid"));
					ab.setAdFileName(resultSet.getString("adFileName"));
					ab.setUrl(resultSet.getString("url"));
					ab.setDisplay(resultSet.getBoolean("display"));
					abList.add(ab);
				}
			}
		}
		return abList;
	}
	
	public AdvertisementBean getSpecificQueryRecordById(int id)
			throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		AdvertisementBean ab = new AdvertisementBean();
		String preparedSQL = "SELECT * FROM advertisements where adid =?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection
						.prepareStatement(preparedSQL);) {
			pStatement.setInt(1, id);

			try (ResultSet resultSet = pStatement.executeQuery();) {
				while (resultSet.next()) {
					
					
					
					ab.setAdId(resultSet.getInt("adid"));
					ab.setAdFileName(resultSet.getString("adFileName"));
					ab.setUrl(resultSet.getString("url"));
					ab.setDisplay(resultSet.getBoolean("display"));
				}
			}
		}
		return ab;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.g3w14.persistence.AdvertisementDAO#deleteRecord(com.g3w14.data.
	 * AdvertisementBean)
	 */
	@Override
	public int deleteRecord(AdvertisementBean ab) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		int records = 0;
		String preparedSQL = "DELETE FROM advertisements WHERE adId = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL)) {
			ps.setInt(1, ab.getAdId());
			records = ps.executeUpdate();
		}

		return records;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.g3w14.persistence.AdvertisementDAO#insertRecord(com.g3w14.data.
	 * AdvertisementBean)
	 */
	@Override
	public int insertRecord(AdvertisementBean ab) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		int records = 0;
		String preparedSQL = "INSERT INTO advertisements "
				+ "(adfilename, url) " + "values (?, ?)";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL)) {
			ps.setString(1, ab.getAdFileName());
			ps.setString(2, ab.getUrl());
			records = ps.executeUpdate();
		}

		return records;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.g3w14.persistence.AdvertisementDAO#updateRecord(com.g3w14.data.
	 * AdvertisementBean)
	 */
	@Override
	public int updateRecord(AdvertisementBean ab) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		int records = 0;
		String preparedSQL = "UPDATE advertisements SET "
				+ "adFileName = ?, url = ?, display = ? WHERE adid = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL)) {
			ps.setString(1, ab.getAdFileName());
			ps.setString(2, ab.getUrl());
			ps.setBoolean(3, ab.isDisplay());
			ps.setInt(4, ab.getAdId());
			
			records = ps.executeUpdate();
		}

		return records;
	}
}