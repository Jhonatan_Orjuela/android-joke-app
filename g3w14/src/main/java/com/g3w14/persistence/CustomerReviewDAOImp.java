package com.g3w14.persistence;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.sql.DataSource;

import com.g3w14.data.ClientBean;
import com.g3w14.data.CustomerReviewBean;

/**
 * This is the implementation class for the CustomerReviewDAO interface.
 * @author Sean-Frankel Gaon Canlas, Theo
 *
 */
@Named("customerReviewAction")
@RequestScoped
public class CustomerReviewDAOImp implements CustomerReviewDAO, Serializable 
{
	@Resource(name = "jdbc/g3w14")
	private DataSource dataSource;

	public CustomerReviewDAOImp() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.g3w14.persistence.CustomerReviewDAO#getQueryRecords()
	 */
	@Override
	public ArrayList<CustomerReviewBean> getQueryRecords() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		ArrayList<CustomerReviewBean> crbList = new ArrayList<>();
		String preparedSQL = "SELECT * FROM customerreview";
		String nameSQL = "SELECT firstname, lastname FROM client WHERE id = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL);
				PreparedStatement ps2 = connection.prepareStatement(nameSQL);
				ResultSet resultSet = ps.executeQuery()) {
			while (resultSet.next()) {
				CustomerReviewBean crb = new CustomerReviewBean();

				crb.setKey(resultSet.getInt("reviewkey"));
				crb.setApproval(resultSet.getBoolean("approval"));
				crb.setDate(resultSet.getTimestamp("date"));
				crb.setIsbn(resultSet.getString("isbn"));
				crb.setClientId(resultSet.getInt("clientid"));
				
				ps2.setInt(1, crb.getClientId());
				try(ResultSet resultSet2 = ps2.executeQuery()) {
					while (resultSet2.next()) {
					String name = resultSet2.getString("firstname") + " " + resultSet2.getString("lastname");
					crb.setName(name);
					}
				}
				crb.setRating(resultSet.getInt("rating"));
				crb.setText(resultSet.getString("text"));

				crbList.add(crb);
			}
		}
		return crbList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.g3w14.persistence.CustomerReviewDAO#getSpecificQueryRecords()
	 */
	@Override
	public ArrayList<CustomerReviewBean> getSpecificQueryRecords(
			CustomerReviewBean crb1) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		ArrayList<CustomerReviewBean> crbList = new ArrayList<>();
		String preparedSQL = "SELECT * FROM customerreview where isbn = ? and approval is not false";
		String nameSQL = "SELECT firstname, lastname FROM client WHERE id = ?";
		
		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL)) {
				PreparedStatement ps2 = connection.prepareStatement(nameSQL);
			ps.setString(1, crb1.getIsbn());

			try (ResultSet resultSet = ps.executeQuery()) {
				while (resultSet.next()) {
					CustomerReviewBean crb = new CustomerReviewBean();

					crb.setKey(resultSet.getInt("reviewkey"));
					crb.setApproval(resultSet.getBoolean("approval"));
					crb.setDate(resultSet.getTimestamp("date"));
					crb.setIsbn(resultSet.getString("isbn"));
					crb.setClientId(resultSet.getInt("clientid"));
					
					ps2.setInt(1, crb.getClientId());
					try(ResultSet resultSet2 = ps2.executeQuery()) {
						while (resultSet2.next()) {
						String name = resultSet2.getString("firstname") + " " + resultSet2.getString("lastname");
						crb.setName(name);
						}
					}
					crb.setRating(resultSet.getInt("rating"));
					crb.setText(resultSet.getString("text"));

					crbList.add(crb);
				}
			}
		}
		return crbList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.g3w14.persistence.CustomerReviewDAO#deleteRecord(int)
	 */
	@Override
	public int deleteRecord(CustomerReviewBean crb) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		int records = 0;
		String preparedSQL = "DELETE FROM CustomerReview WHERE reviewkey = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL)) {
			ps.setInt(1, crb.getKey());
			records = ps.executeUpdate();
		}

		return records;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.g3w14.persistence.CustomerReviewDAO#insertRecord(com.g3w14.data.
	 * CustomerReviewBean)
	 */
	@Override
	public int insertRecord(CustomerReviewBean crb) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		int records = 0;
		String preparedSQL = "INSERT INTO CustomerReview "
				+ "(isbn, name, date, clientid, rating, text, approval) "
				+ "values (?,?,?,?,?,?,?)";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL)) {
			ps.setString(1, crb.getIsbn());
			ps.setString(2, crb.getName());
			ps.setTimestamp(3, new Timestamp((new java.util.Date()).getTime()));
			ps.setInt(4, crb.getClientId());
			ps.setInt(5, crb.getRating());
			ps.setString(6, crb.getText());
			ps.setBoolean(7, false);
			records = ps.executeUpdate();
		}

		return records;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.g3w14.persistence.CustomerReviewDAO#updateRecord(com.g3w14.data.
	 * CustomerReviewBean)
	 */
	@Override
	public int updateRecord(CustomerReviewBean crb) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		int records = 0;
		String preparedSQL = "UPDATE CustomerReview SET "
				+ "isbn = ?, date = ?, name = ?, rating = ?, text = ?, approval = ? "
				+ "where reviewkey = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL)) {
			ps.setString(1, crb.getIsbn());
			ps.setTimestamp(2, crb.getDate());
			ps.setInt(3, crb.getClientId());
			ps.setInt(4, crb.getRating());
			ps.setString(5, crb.getText());
			ps.setBoolean(6, crb.getApproval());
			ps.setInt(7, crb.getKey());
			records = ps.executeUpdate();
		}

		return records;
	}

	@Override
	public CustomerReviewBean getSingleReview(int key) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		CustomerReviewBean crb = null;
		String preparedSQL = "SELECT * FROM customerreview where reviewkey = ?";
		String nameSQL = "SELECT firstname, lastname FROM client WHERE id = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL);
				PreparedStatement ps2 = connection.prepareStatement(nameSQL);
				) {
			ps.setInt(1, key);
			ResultSet resultSet = ps.executeQuery();
			while (resultSet.next()) {
				crb = new CustomerReviewBean();

				crb.setKey(resultSet.getInt("reviewkey"));
				crb.setApproval(resultSet.getBoolean("approval"));
				crb.setDate(resultSet.getTimestamp("date"));
				crb.setIsbn(resultSet.getString("isbn"));
				crb.setClientId(resultSet.getInt("clientid"));
				
				ps2.setInt(1, crb.getClientId());
				try(ResultSet resultSet2 = ps2.executeQuery()) {
					while (resultSet2.next()) {
					String name = resultSet2.getString("firstname") + " " + resultSet2.getString("lastname");
					crb.setName(name);
					}
				}
				crb.setRating(resultSet.getInt("rating"));
				crb.setText(resultSet.getString("text"));
			}
		}
		return crb;
	}
	
	public CustomerReviewBean getSingleReviewbyISBN(CustomerReviewBean abean) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		
		CustomerReviewBean crb = null;
		String preparedSQL = "SELECT * FROM customerreview where isbn = ? and approval is not false";
		String nameSQL = "SELECT firstname, lastname FROM client WHERE id = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL);
				PreparedStatement ps2 = connection.prepareStatement(nameSQL);
				) {
			ps.setString(1, abean.getIsbn());
			ResultSet resultSet = ps.executeQuery();
			while (resultSet.next()) {
				crb = new CustomerReviewBean();

				crb.setKey(resultSet.getInt("reviewkey"));
				crb.setApproval(resultSet.getBoolean("approval"));
				crb.setDate(resultSet.getTimestamp("date"));
				crb.setIsbn(resultSet.getString("isbn"));
				crb.setClientId(resultSet.getInt("clientid"));
				
				ps2.setInt(1, crb.getClientId());
				try(ResultSet resultSet2 = ps2.executeQuery()) {
					while (resultSet2.next()) {
					String name = resultSet2.getString("firstname") + " " + resultSet2.getString("lastname");
					crb.setName(name);
					}
				}
				crb.setRating(resultSet.getInt("rating"));
				crb.setText(resultSet.getString("text"));
			}
		}
		return crb;
	}
}