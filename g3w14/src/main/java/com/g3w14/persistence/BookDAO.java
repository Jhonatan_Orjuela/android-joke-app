package com.g3w14.persistence;

import java.sql.SQLException;
import java.util.ArrayList;

import com.g3w14.data.BookBean;

/**
 * Mandates the function of Book Data Access Objects to query the records
 * in the Book table along with other forms of data manipulation.
 * 
 * @author Tyler Patricio
 */
public interface BookDAO
{
	public ArrayList<BookBean> getAllRecords() throws SQLException;
	public ArrayList<BookBean> advancedSearch(BookBean bb) throws SQLException;
	public int deleteRecord(String isbn13) throws SQLException;
	public int insertRecord(BookBean bb) throws SQLException;
	public int updateRecord(BookBean bb) throws SQLException;
	public ArrayList<BookBean> retriveBestSellers() throws SQLException ;
	public BookBean getQuerySingleBook(String isbn13) throws SQLException;
	public ArrayList<String> getAllAuthor() throws SQLException;
	public ArrayList<String> getAllPublisher() throws SQLException;
	public ArrayList<BookBean> getZeroSales(String date1, String date2) throws SQLException;
	public ArrayList<BookBean> getReorder() throws SQLException;
	public ArrayList<BookBean> getBooksOnSale() throws SQLException;
	public ArrayList<BookBean> getTopSellers() throws SQLException;

}