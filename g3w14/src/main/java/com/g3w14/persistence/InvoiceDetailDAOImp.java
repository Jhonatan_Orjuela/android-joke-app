package com.g3w14.persistence;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;

import com.g3w14.data.InvoiceDetailBean;

/**
 * Implementation class that enables the ability to manipulate and query records from 
 * the InvoiceDetail table.
 * 
 * @author Théo
 *
 */
@Named("invoiceDetailAction")
@RequestScoped
public class InvoiceDetailDAOImp implements InvoiceDetailDAO, Serializable {

	@Resource(name = "jdbc/g3w14")
	private DataSource dataSource;

	public InvoiceDetailDAOImp() {
		super();
	}

	@Override
	public ArrayList<InvoiceDetailBean> getQueryRecords() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<InvoiceDetailBean> invoiceDetailList = new ArrayList<InvoiceDetailBean>();
		String preparedQuery = "SELECT * FROM INVOICEDETAIL";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection
						.prepareStatement(preparedQuery);
				ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				InvoiceDetailBean invoiceDetail = new InvoiceDetailBean();
				invoiceDetail.setId(rs.getInt("id"));
				invoiceDetail.setSaleNumber(rs.getInt("salenumber"));
				invoiceDetail.setQuantity(rs.getInt("quantity"));
				invoiceDetail.setBookIsbn(rs.getString("bookisbn"));
				invoiceDetail.setBookPrice(rs.getBigDecimal("bookprice"));
				invoiceDetailList.add(invoiceDetail);
			}
		}
		return invoiceDetailList;
	}

	@Override
	public InvoiceDetailBean extractSingleInvoiceDetail(int invoicedId)
			throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		InvoiceDetailBean invoiced = new InvoiceDetailBean();
		String preparedQuery = "SELECT * FROM INVOICEDETAIL WHERE id = ? ";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection
						.prepareStatement(preparedQuery)) {
			ps.setInt(1, invoicedId);
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next())
				{
					invoiced.setId(rs.getInt("id"));
					invoiced.setSaleNumber(rs.getInt("salenumber"));
					invoiced.setQuantity(rs.getInt("quantity"));
					invoiced.setBookIsbn(rs.getString("bookisbn"));
					invoiced.setBookPrice(rs.getBigDecimal("bookprice"));
				}
			}
		}
		return invoiced;
	}

	@Override
	public int insertRecord(InvoiceDetailBean indvb) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String preparedQuery = "INSERT INTO INVOICEDETAIL (id, salenumber, quantity,"
				+ "bookisbn, bookprice) VALUES (?,?,?,?,?)";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection
						.prepareStatement(preparedQuery);) {

			ps.setInt(1, indvb.getId());
			ps.setInt(2, indvb.getSaleNumber());
			ps.setInt(3, indvb.getQuantity());
			ps.setString(4, indvb.getBookIsbn());
			ps.setBigDecimal(5, indvb.getBookPrice());
			return ps.executeUpdate();
		}
	}

	@Override
	public int updateRecord(InvoiceDetailBean indvb) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String preparedQuery = "UPDATE INVOICEDETAIL SET "
				+ "id = ?, salenumber = ?, quantity = ?, bookisbn = ?, bookprice = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection
						.prepareStatement(preparedQuery);) {
			ps.setInt(1, indvb.getId());
			ps.setInt(2, indvb.getSaleNumber());
			ps.setInt(3, indvb.getQuantity());
			ps.setString(4, indvb.getBookIsbn());
			ps.setBigDecimal(5, indvb.getBookPrice());
			return ps.executeUpdate();
		}
	}

	@Override
	public int deleteRecord(InvoiceDetailBean idb) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int records = 0;
		String preparedSQL = "DELETE FROM invoicedetail WHERE id = ?";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL)) {
			ps.setInt(1, idb.getId());
			records = ps.executeUpdate();
		}
		return records;
	}

	@Override
	public ArrayList<InvoiceDetailBean> getQueryRecordsByAuthor(String author, String date1, String date2)
			throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<InvoiceDetailBean> invoiceDetailList = new ArrayList<InvoiceDetailBean>();
		if(date1 == null | date1 == "" | date2 == null | date2 == ""){
			String preparedQuery = "SELECT DISTINCT(I.BOOKISBN), sum(I.quantity) as quantity, I.bookprice FROM INVOICEDETAIL I, BOOK B WHERE i.bookisbn = b.isbn13 AND b.author = ? GROUP BY I.BOOKISBN";
			try (Connection connection = dataSource.getConnection();
					PreparedStatement ps = connection
							.prepareStatement(preparedQuery);
					) {
				ps.setString(1, author);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					InvoiceDetailBean invoiceDetail = new InvoiceDetailBean();

					invoiceDetail.setBookIsbn(rs.getString("bookisbn"));
					invoiceDetail.setQuantity(rs.getInt("quantity"));

					invoiceDetail.setBookPrice(rs.getBigDecimal("bookprice"));
					invoiceDetailList.add(invoiceDetail);
				}
			}
			return invoiceDetailList;
		}
		else{
			String preparedQuery = "SELECT DISTINCT(I.BOOKISBN), sum(I.quantity) as quantity, I.bookprice FROM INVOICEDETAIL I, BOOK B WHERE i.bookisbn = b.isbn13 AND b.author = ?  and I.salenumber in ( select salenumber from invoice where dateofsale between ? and ?) GROUP BY I.BOOKISBN";
			try (Connection connection = dataSource.getConnection();
					PreparedStatement ps = connection
							.prepareStatement(preparedQuery);
					) {
				ps.setString(1, author);
				ps.setString(2, date1);
				ps.setString(3, date2);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					InvoiceDetailBean invoiceDetail = new InvoiceDetailBean();

					invoiceDetail.setBookIsbn(rs.getString("bookisbn"));
					invoiceDetail.setQuantity(rs.getInt("quantity"));

					invoiceDetail.setBookPrice(rs.getBigDecimal("bookprice"));
					invoiceDetailList.add(invoiceDetail);
				}
			}
			return invoiceDetailList;						
		}
	}

	@Override
	public ArrayList<InvoiceDetailBean> getQueryRecordsByPublisher(
			String publisher, String date1, String date2) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<InvoiceDetailBean> invoiceDetailList = new ArrayList<InvoiceDetailBean>();
		if(date1 == null | date1 == "" | date2 == null | date2 == ""){
			String preparedQuery = "SELECT DISTINCT(I.BOOKISBN), sum(I.quantity) as quantity, I.bookprice FROM INVOICEDETAIL I, BOOK B WHERE i.bookisbn = b.isbn13 AND b.publisher = ? GROUP BY I.BOOKISBN";
			try (Connection connection = dataSource.getConnection();
					PreparedStatement ps = connection
							.prepareStatement(preparedQuery);
					) {
				ps.setString(1, publisher);

				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					InvoiceDetailBean invoiceDetail = new InvoiceDetailBean();
					invoiceDetail.setBookIsbn(rs.getString("bookisbn"));
					invoiceDetail.setQuantity(rs.getInt("quantity"));

					invoiceDetail.setBookPrice(rs.getBigDecimal("bookprice"));
					invoiceDetailList.add(invoiceDetail);
				}
			}
			return invoiceDetailList;
		}
		else{

			String preparedQuery = "SELECT DISTINCT(I.BOOKISBN), sum(I.quantity) as quantity, I.bookprice FROM INVOICEDETAIL I, BOOK B WHERE i.bookisbn = b.isbn13 AND b.publisher = ? and I.salenumber in ( select salenumber from invoice where dateofsale between ? and ?) GROUP BY I.BOOKISBN";
			try (Connection connection = dataSource.getConnection();
					PreparedStatement ps = connection
							.prepareStatement(preparedQuery);
					) {
				ps.setString(1, publisher);
				ps.setString(2, date1);
				ps.setString(3, date2);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					InvoiceDetailBean invoiceDetail = new InvoiceDetailBean();
					invoiceDetail.setBookIsbn(rs.getString("bookisbn"));
					invoiceDetail.setQuantity(rs.getInt("quantity"));

					invoiceDetail.setBookPrice(rs.getBigDecimal("bookprice"));
					invoiceDetailList.add(invoiceDetail);
				}
			}
			return invoiceDetailList;

		}
	}

	@Override
	public ArrayList<InvoiceDetailBean> getTopSellers(String date1, String date2) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<InvoiceDetailBean> invoiceDetailList = new ArrayList<InvoiceDetailBean>();
		if(date1 == null | date1 == "" | date2 == null | date2 == ""){
			System.out.println("date null");
			String preparedQuery = "SELECT DISTINCT(I.BOOKISBN), sum(I.quantity) as quantity, I.bookprice FROM INVOICEDETAIL I GROUP BY I.BOOKISBN ORDER BY QUANTITY DESC";
			try (Connection connection = dataSource.getConnection();
					PreparedStatement ps = connection
							.prepareStatement(preparedQuery);
					) {
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					InvoiceDetailBean invoiceDetail = new InvoiceDetailBean();
					invoiceDetail.setBookIsbn(rs.getString("bookisbn"));
					invoiceDetail.setQuantity(rs.getInt("quantity"));

					invoiceDetail.setBookPrice(rs.getBigDecimal("bookprice"));
					invoiceDetailList.add(invoiceDetail);
				}
			}
			return invoiceDetailList;
		}
		else{
			System.out.println("DATEDATEDATEDATEDATEDATE");
			String preparedQuery = "SELECT DISTINCT(I.BOOKISBN), sum(I.quantity) as quantity, I.bookprice FROM INVOICEDETAIL I WHERE I.salenumber in ( select salenumber from invoice where dateofsale between ? and ?) GROUP BY I.BOOKISBN ORDER BY QUANTITY DESC";
			try (Connection connection = dataSource.getConnection();
					PreparedStatement ps = connection
							.prepareStatement(preparedQuery);
					) {
				ps.setString(1, date1);
				ps.setString(2, date2);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					InvoiceDetailBean invoiceDetail = new InvoiceDetailBean();
					invoiceDetail.setBookIsbn(rs.getString("bookisbn"));
					invoiceDetail.setQuantity(rs.getInt("quantity"));

					invoiceDetail.setBookPrice(rs.getBigDecimal("bookprice"));
					invoiceDetailList.add(invoiceDetail);
				}
			}
			return invoiceDetailList;
		}
	}

	@Override
	public ArrayList<InvoiceDetailBean> getInvoiceDetailPerInvoice(
			int invoicenumber) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		System.out.println(" SALENUMBER " + invoicenumber);
		ArrayList<InvoiceDetailBean> invoiceDetailList = new ArrayList<InvoiceDetailBean>();
		String preparedQuery = "SELECT DISTINCT(BOOKISBN), ID, SALENUMBER, SUM(QUANTITY) AS QUANTITY, BOOKPRICE FROM INVOICEDETAIL WHERE SALENUMBER = ? GROUP BY BOOKISBN";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection
						.prepareStatement(preparedQuery);
				) {
			ps.setInt(1, invoicenumber);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				InvoiceDetailBean invoiceDetail = new InvoiceDetailBean();
				invoiceDetail.setId(rs.getInt("id"));
				invoiceDetail.setSaleNumber(rs.getInt("salenumber"));
				invoiceDetail.setQuantity(rs.getInt("quantity"));
				invoiceDetail.setBookIsbn(rs.getString("bookisbn"));
				invoiceDetail.setBookPrice(rs.getBigDecimal("bookprice"));
				invoiceDetailList.add(invoiceDetail);
			}
		}
		if(invoiceDetailList.size() > 0)
		System.out.println(invoiceDetailList.get(0).toString());
		return invoiceDetailList;
	}


}


