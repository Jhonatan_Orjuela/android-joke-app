package com.g3w14.persistence;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.sql.DataSource;

import com.g3w14.data.ClientBean;
import com.g3w14.data.InvoiceBean;
import com.g3w14.data.PasswordPBKDF2;

/**
 * ClientDAOImp
 * 
 * This class interacts with the database through sql statements created with
 * PreparedStatement class. It then returns a result set or the number of rows
 * effected.
 * 
 * @author Sophie Leduc Major 0931442
 * 
 */

@Named("clientAction")
@RequestScoped
public class ClientDAOImp implements ClientDAO, Serializable {
	
	@Resource(name = "jdbc/g3w14")
	private DataSource dataSource;

	public ClientDAOImp() {
		super();
	}

	/**
	 * getQueryClients
	 * 
	 * This method returns all the clients from the database.
	 * 
	 * @return ArratList<ClientBean> rows: arraylist containing all the clients.
	 * @throws SQLException
	 * 
	 */
	@Override
	public ArrayList<ClientBean> getQueryClients() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String sql = "SELECT * FROM client";
		ArrayList<ClientBean> rows = new ArrayList<>();

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);
				ResultSet resultSet = pStatement.executeQuery()) {
			while (resultSet.next()) {

				ClientBean c = new ClientBean();

				c.setId(resultSet.getInt("id"));
				c.setEmail(resultSet.getString("email"));
				c.setPassword(resultSet.getString("password"));
				c.setTitle(resultSet.getString("title"));
				c.setFirstName(resultSet.getString("firstname"));
				c.setLastName(resultSet.getString("lastname"));
				c.setCompany(resultSet.getString("company"));
				c.setAddress1(resultSet.getString("address1"));
				c.setAddress2(resultSet.getString("address2"));
				c.setCity(resultSet.getString("city"));
				c.setProvince(resultSet.getString("province"));
				c.setCountry(resultSet.getString("country"));
				c.setPostalCode(resultSet.getString("postalCode"));
				c.setHomeTelephone(resultSet.getString("hometelephone"));
				c.setCellTelephone(resultSet.getString("celltelephone"));
				c.setLastSearch(resultSet.getString("lastsearch"));
				c.setAdmin(resultSet.getInt("admin"));
				rows.add(c);
			}
		}

		return rows;

	}

	/**
	 * getQuerySingleClient
	 * 
	 * This method returns a single client found with its unique client id
	 * 
	 * @return ClientBean row: client bean with the matching email.
	 * @throws SQLException
	 * 
	 */
	@Override
	public ClientBean getQuerySingleClient(String email)
			throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String sql = "SELECT * FROM client WHERE email = ?";
		ArrayList<ClientBean> rows = new ArrayList<ClientBean>();

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setString(1, email);

			try (ResultSet resultSet = pStatement.executeQuery();) {
				while (resultSet.next()) {

					ClientBean c = new ClientBean();

					c.setId(resultSet.getInt("id"));
					c.setEmail(resultSet.getString("email"));
					c.setPassword(resultSet.getString("password"));	
					c.setTitle(resultSet.getString("title"));
					c.setFirstName(resultSet.getString("firstname"));
					c.setLastName(resultSet.getString("lastname"));
					c.setCompany(resultSet.getString("company"));
					c.setAddress1(resultSet.getString("address1"));
					c.setAddress2(resultSet.getString("address2"));
					c.setCity(resultSet.getString("city"));
					c.setProvince(resultSet.getString("province"));
					c.setCountry(resultSet.getString("country"));
					c.setPostalCode(resultSet.getString("postalCode"));
					c.setHomeTelephone(resultSet.getString("hometelephone"));
					c.setCellTelephone(resultSet.getString("celltelephone"));
					c.setLastSearch(resultSet.getString("lastsearch"));
					c.setAdmin(resultSet.getInt("admin"));
					rows.add(c);
				}
			}
		}

		if(rows.size()!=1)
			rows.add(null);

		return rows.get(0);
	}

	/**
	 * insertClient
	 * 
	 * This inserts a client bean into the database.
	 * 
	 * @return record: number of rows effected.
	 * @throws SQLException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 * 
	 */
	@Override
	public int insertClient(ClientBean c) throws SQLException,
	NoSuchAlgorithmException, InvalidKeySpecException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int record;

		String preparedQuery = "INSERT INTO client (email, password, title, lastname, firstname, company, address1,"
				+ " address2, city, province, country, postalcode, hometelephone,"
				+ " celltelephone, lastsearch, admin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection
						.prepareStatement(preparedQuery);) {

			String password = PasswordPBKDF2.createPassword(c.getPassword());

			pStatement.setString(1, c.getEmail());
			pStatement.setString(2, password);
			pStatement.setString(3, c.getTitle());
			pStatement.setString(4, c.getLastName());
			pStatement.setString(5, c.getFirstName());
			pStatement.setString(6, c.getCompany());
			pStatement.setString(7, c.getAddress1());
			pStatement.setString(8, c.getAddress2());
			pStatement.setString(9, c.getCity());
			pStatement.setString(10, c.getProvince());
			pStatement.setString(11, c.getCountry());
			pStatement.setString(12, c.getPostalCode());
			pStatement.setString(13, c.getHomeTelephone());
			pStatement.setString(14, c.getCellTelephone());
			pStatement.setString(15, c.getLastSearch());
			pStatement.setInt(16, c.getAdmin());
			record = pStatement.executeUpdate();
		}

		return record;
	}

	/**
	 * updateClient
	 * 
	 * This updates an existing client bean from the database with new values.
	 * 
	 * @return record: number of rows effected.
	 * @throws SQLException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 * 
	 */
	@Override
	public int updateClient(ClientBean c) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int record;

		String preparedQuery = "UPDATE client SET email = ?, title = ?, lastname = ?, firstname = ?,"
				+ "company = ?, address1 = ?, address2 = ?, city = ?, province = ?, postalcode = ?, country = ?,"
				+ " hometelephone = ?, celltelephone = ?, lastsearch = ?, admin= ? WHERE id = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection
						.prepareStatement(preparedQuery);) {

			pStatement.setString(1, c.getEmail());
			pStatement.setString(2, c.getTitle());
			pStatement.setString(3, c.getLastName());
			pStatement.setString(4, c.getFirstName());
			pStatement.setString(5, c.getCompany());
			pStatement.setString(6, c.getAddress1());
			pStatement.setString(7, c.getAddress2());
			pStatement.setString(8, c.getCity());
			pStatement.setString(9, c.getProvince());
			pStatement.setString(10, c.getCountry());
			pStatement.setString(11, c.getPostalCode());
			pStatement.setString(12, c.getHomeTelephone());
			pStatement.setString(13, c.getCellTelephone());
			pStatement.setString(14, c.getLastSearch());
			pStatement.setInt(15, c.getAdmin());
			pStatement.setInt(16, c.getId());

			record = pStatement.executeUpdate();
		}

		return record;
	}

	/**
	 * deleteClient
	 * 
	 * This deletes a client bean from the database using its unique client id.
	 * 
	 * @return record: number of rows effected.
	 * @throws SQLException
	 * 
	 */
	@Override
	public int deleteClient(int c) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int record;

		String sql = "DELETE FROM client WHERE id = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setInt(1, c);
			record = pStatement.executeUpdate();
		}

		return record;
	}

	/**
	 * updateClient
	 * 
	 * This updates a client bean from the database.
	 * 
	 * @return record: number of rows effected.
	 * @throws SQLException
	 * 
	 */
	@Override
	public int updateClientPassword(ClientBean c)
			throws NoSuchAlgorithmException, InvalidKeySpecException,
			SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int record;

		String preparedQuery = "UPDATE client SET password = ?	WHERE id = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection
						.prepareStatement(preparedQuery);) {
			String password = PasswordPBKDF2.createPassword(c.getPassword());

			pStatement.setString(1, password);
			pStatement.setInt(2, c.getId());

			record = pStatement.executeUpdate();
		}

		return record;
	}
	
	@Override
	public int updateClientEmail(ClientBean c) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		int record;

		String preparedQuery = "UPDATE client SET email = ?	WHERE id = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection
						.prepareStatement(preparedQuery);) {

			pStatement.setString(1, c.getEmail());
			pStatement.setInt(2, c.getId());

			record = pStatement.executeUpdate();
		}

		return record;
	}
	

	@Override
	public Boolean clientExist(String email) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String sql = "SELECT * FROM client WHERE email = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setString(1, email);

			try (ResultSet resultSet = pStatement.executeQuery();) {
				if(resultSet.next()){
					return true;
				}
				return false;
			}
		}

	}

	@Override
	public Boolean logionSuccess(String email, String Password) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException{
		String hashpassword;
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String sql = "SELECT password FROM client WHERE email = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setString(1, email);

			try (ResultSet resultSet = pStatement.executeQuery();) {
				if(resultSet.next()){
					hashpassword = resultSet.getString("password");
					if(PasswordPBKDF2.validatePassword(Password, hashpassword)){
						return true;
					}
					else{ 
						return false;
					}
				}
				return false;
			}
		}

	}

	@Override
	public ClientBean getQuerySingleClientId(int id) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String sql = "SELECT * FROM client WHERE id = ?";
		ArrayList<ClientBean> rows = new ArrayList<ClientBean>();

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setInt(1, id);

			try (ResultSet resultSet = pStatement.executeQuery();) {
				while (resultSet.next()) {

					ClientBean c = new ClientBean();

					c.setId(resultSet.getInt("id"));
					c.setEmail(resultSet.getString("email"));
					c.setPassword(resultSet.getString("password"));	
					c.setTitle(resultSet.getString("title"));
					c.setFirstName(resultSet.getString("firstname"));
					c.setLastName(resultSet.getString("lastname"));
					c.setCompany(resultSet.getString("company"));
					c.setAddress1(resultSet.getString("address1"));
					c.setAddress2(resultSet.getString("address2"));
					c.setCity(resultSet.getString("city"));
					c.setProvince(resultSet.getString("province"));
					c.setCountry(resultSet.getString("country"));
					c.setPostalCode(resultSet.getString("postalCode"));
					c.setHomeTelephone(resultSet.getString("hometelephone"));
					c.setCellTelephone(resultSet.getString("celltelephone"));
					c.setLastSearch(resultSet.getString("lastsearch"));
					c.setAdmin(resultSet.getInt("admin"));
					rows.add(c);
				}
			}
		}

		if(rows.size()!=1)
			rows.add(null);

		return rows.get(0);
	}

}