/**
 * 
 */
package com.g3w14.persistence;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;

import com.g3w14.data.RSSBean;
import com.g3w14.data.SurveyBean;

/**
 * the persistence layer for our RSS
 * @author 1040134 Jhonatan Orjuela
 *
 */
@Named("RSSPersistence")
@RequestScoped
public class RSSDAOImp implements Serializable {

	@Resource(name = "jdbc/g3w14")
	private DataSource dataSource;
	/**
	 * 
	 */
	public RSSDAOImp() {
		super();
	}

	public ArrayList<RSSBean> getRSSRecords() throws SQLException {
		if(dataSource ==null)
			throw new SQLException("ERROR, Can't get the data source");
		
		String sql = "SELECT * FROM newsfeed";
		
		ArrayList<RSSBean> rssBeans = new ArrayList();
		try(Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);
				ResultSet rs = ps.executeQuery()) {
			while(rs.next()){
				RSSBean aBean = new RSSBean();
				aBean.setId(rs.getInt("id"));
				aBean.setDisplay(rs.getBoolean("display"));
				aBean.setRssUrl(rs.getString("rssurl"));
				rssBeans.add(aBean);
			}
		}
		
		return rssBeans;
	}

	public RSSBean getSingleRSS(int RSSId) throws SQLException {
		
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		RSSBean rssBean = new RSSBean();
		String sql = "SELECT * FROM newsfeed WHERE id = ?";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pS = connection.prepareStatement(sql);
				) {
			pS.setInt(1, RSSId);
			ResultSet rs = pS.executeQuery();
			rs.next();
			rssBean.setId(rs.getInt("id"));
			rssBean.setDisplay(rs.getBoolean("display"));
			rssBean.setRssUrl(rs.getString("rssurl"));
			
		}
	return rssBean;
	}
	
	public int insertRSS(RSSBean rss) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int records = -1;

		String insertStmt = "INSERT INTO newsfeed(rssurl, display)"
				+ "VALUES(?,?)";
		try(Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(insertStmt);){
			ps.setString(1, rss.getRssUrl());
			ps.setBoolean(2, false);
			records = ps.executeUpdate();
		
		}
		return records;
	}
	
	public int changeRSSDisplaysToFalse() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int records = -1;
		
		String updateStmt = "UPDATE newsfeed SET display = ? WHERE id = *";
		try(Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(updateStmt);
				) {
			ps.setBoolean(1, false);
			
			records = ps.executeUpdate();
		}
		return records;
		
	}
	
	public int updateRSS(RSSBean aBean)throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int records = -1;
		
		String updateStmt = "UPDATE newsfeed SET display = ?, rssurl = ? WHERE id = ?";
		try(Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(updateStmt);
				) {
			ps.setBoolean(1, aBean.isDisplay());
			ps.setString(2, aBean.getRssUrl());
			ps.setInt(3, aBean.getId());
			
			records = ps.executeUpdate();
		}
		return records;
		
	}
	
	public int deleteRSS(RSSBean aBean) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int records = -1;

		String deleteStatement = "DELETE FROM newsfeed " + "WHERE id = ?";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pS = connection
						.prepareStatement(deleteStatement);) {

			pS.setInt(1, aBean.getId());

			records = pS.executeUpdate();
		}

		return records;
	}
	
}
