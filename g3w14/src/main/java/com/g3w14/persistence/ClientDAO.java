package com.g3w14.persistence;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.ArrayList;

import com.g3w14.data.ClientBean;
import com.g3w14.data.InvoiceBean;

/**		ClientDAO Interface
 * 
 * Mandates the function of Client Data Access Objects to query the records
 * in the Client table along with other forms of data manipulation.
 * 
 * @author Sophie Leduc Major (0931442)
 *
 */
public interface ClientDAO {

	public Boolean clientExist(String email) throws SQLException;
	public Boolean logionSuccess(String email, String Password) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException;
	public ArrayList<ClientBean> getQueryClients() throws SQLException;
	public ClientBean getQuerySingleClient(String email) throws SQLException;
	public ClientBean getQuerySingleClientId(int id) throws SQLException;
	public int insertClient(ClientBean c) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException;
	public int updateClient(ClientBean c) throws SQLException;
	public int updateClientPassword(ClientBean c) throws NoSuchAlgorithmException, InvalidKeySpecException, SQLException;
	public int updateClientEmail(ClientBean c) throws SQLException;
	public int deleteClient(int c) throws SQLException;
}