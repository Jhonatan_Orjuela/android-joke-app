/**
 * 
 */
package com.g3w14.persistence;

import java.sql.SQLException;
import java.util.ArrayList;

import com.g3w14.data.SurveyBean;

/**
 * Mandates the function of Survey Data Access Objects to query the records
 * in the Survey table along with other forms of data manipulation.
 * 
 * @author 1040134
 *
 */
public interface SurveyDAO {
	
	/**
	 * extracts the full list of surveys
	 * @return The list of extracted surveys
	 * @throws SQLException
	 */
	public ArrayList<SurveyBean> getSurveyRecords() throws SQLException;
	
	/**
	 * extracts a single survey 
	 * @param surveyId the id of the survey
	 * @return the survey extracted
	 * @throws SQLException
	 */
	public SurveyBean getSingleSurvey(int surveyId) throws SQLException;
	
	/**
	 * inserts survey into database
	 * @param survey the survey to be inserted
	 * @return The number of records inserted
	 * @throws SQLException
	 */
	public int insertSurvey(SurveyBean survey) throws SQLException;
	
	/**
	 * Updates a survey in the Database
	 * @param updatedSurvey the survey to update
	 * @return the number of records altered
	 * @throws SQLException
	 */
	public int updateSurvey(SurveyBean updatedSurvey) throws SQLException;
	
	/**
	 * deletes a survey in the database
	 * @param survey the survey to delete
	 * @return the number of records updates
	 * @throws SQLException
	 */
	public int deleteSurvey(SurveyBean survey) throws SQLException;
	
	
	
}
