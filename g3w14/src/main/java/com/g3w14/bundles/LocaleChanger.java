package com.g3w14.bundles;

import java.io.Serializable;
import java.util.Locale;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;


/**
 * Class that enables our web site to change the default language.
 * 
 * @author Tyler Patricio, Sean-Frankel Gaon Canlas
 */

@Named("localeChanger")
@SessionScoped
public class LocaleChanger implements Serializable {

	private Locale locale = new Locale("en_CA");

	private String localeCode = locale.getLanguage();
	
	private static final long serialVersionUID = -2488850694661474403L;

	public LocaleChanger() {
		super();
	}

	public String changeToFrench() {
		setLocale(Locale.CANADA_FRENCH);
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
		
		return "";
	}

	public String changeToEnglish() {
		setLocale(Locale.CANADA);
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
		
		return "";
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public String getLocaleCode() {
		return localeCode;
	}
	
	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}
}