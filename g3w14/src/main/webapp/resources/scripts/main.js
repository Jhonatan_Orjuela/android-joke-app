/*
 * Author: Sean-Frankel Gaon Canlas
 */

$(document).ready(function(){

	// Placeholders for all the input fields on the website.
	$(".book-search").attr("placeholder", "Game of Thrones");	
	$(".input-email").attr("placeholder", "Email address");	
	$(".input-password").attr("placeholder", "Password");	
	$("#signupform\\:password1").attr("placeholder", "Password");	
	$("#signupform\\:email1").attr("placeholder", "john@email.com");
	$("#signupform\\:firstname").attr("placeholder", "John");	
	$("#signupform\\:lastname").attr("placeholder", "Smith");	
	$("#signupform\\:company").attr("placeholder", "BookBay Not Incorporated");
	$("#signupform\\:address1").attr("placeholder", "1234 By The Bay Avenue");	
	$("#signupform\\:address2").attr("placeholder", "1337th Avenue");	
	$("#signupform\\:city").attr("placeholder", "Montreal");	
	$("#signupform\\:postal").attr("placeholder", "A1A 2E2");
	$("#signupform\\:cellphone").attr("placeholder", "514-123-4567");
	$("#signupform\\:hometelephone").attr("placeholder", "514-123-4567");

	$("#change-pass-form\\:password1").attr("placeholder", "Password");	
	$("#change-email-form\\:email1").attr("placeholder", "john@email.com");
	
	// Setup drop down menu
	$('.dropdown1').dropdown();

	// Fix input element click problem
	$('.dropdown1 input').click(function(e) {
		e.stopPropagation();
	});

	// Carousel
	$('.bxslider').bxSlider({
		infiniteLoop: false,
		hideControlOnEnd: true,
		auto: true,
		autoHover: true,
		pager: false,
		preloadImages: "all",
		slideWidth: 350,
		maxSlides: 3,
		minSlides: 1
	});

	$('.recent-views').bxSlider({
		infiniteLoop: false,
		hideControlOnEnd: true,
		pager: false,
		slideWidth: 350,
		maxSlides: 3,
		minSlides: 1
	});
});