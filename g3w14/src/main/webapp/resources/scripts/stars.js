/**
 * This JavaScript file uses jQuery to do all the mouseover, mouseout, click
 * with the review stars. Base structure of this made by Jhonatan Orjuela.
 * Left over madness with the IF Krakens done by Sean-Frankel Gaon Canlas.
 * Author: Sean-Frankel Gaon Canlas, Jhonatan Orjuela
 */

$("#review-form\\:rating_1").click(function() {
	$(this).data('clicked', true);

	$("#review-form\\:rating_2").data('clicked', false);
	$("#review-form\\:rating_3").data('clicked', false);
	$("#review-form\\:rating_4").data('clicked', false);
	$("#review-form\\:rating_5").data('clicked', false);

	$(this).attr("src", "resources/images/star.ico");
});
$("#review-form\\:rating_1").mouseover(function() {
	$(this).attr("src", "resources/images/star.ico");

	$("#review-form\\:rating_2").attr("src", "resources/images/grey-star.ico");
	$("#review-form\\:rating_3").attr("src", "resources/images/grey-star.ico");
	$("#review-form\\:rating_4").attr("src", "resources/images/grey-star.ico");
	$("#review-form\\:rating_5").attr("src", "resources/images/grey-star.ico");
}).mouseout(function() {
	if ($(this).data("clicked") == false || $(this).data("clicked") == null) {
		$(this).attr("src", "resources/images/grey-star.ico");
	}
	if ($("#review-form\\:rating_5").data("clicked") == true) {
		$("#review-form\\:rating_5").attr("src", "resources/images/star.ico");
		$("#review-form\\:rating_4").attr("src", "resources/images/star.ico");
		$("#review-form\\:rating_3").attr("src", "resources/images/star.ico");
		$("#review-form\\:rating_2").attr("src", "resources/images/star.ico");
		$("#review-form\\:rating_1").attr("src", "resources/images/star.ico");
	}
	if ($("#review-form\\:rating_4").data("clicked") == true) {
		$("#review-form\\:rating_4").attr("src", "resources/images/star.ico");
		$("#review-form\\:rating_3").attr("src", "resources/images/star.ico");
		$("#review-form\\:rating_2").attr("src", "resources/images/star.ico");
		$("#review-form\\:rating_1").attr("src", "resources/images/star.ico");
	}
	if ($("#review-form\\:rating_3").data("clicked") == true) {
		$("#review-form\\:rating_3").attr("src", "resources/images/star.ico");
		$("#review-form\\:rating_2").attr("src", "resources/images/star.ico");
		$("#review-form\\:rating_1").attr("src", "resources/images/star.ico");
	}
	if ($("#review-form\\:rating_2").data("clicked") == true) {
		$("#review-form\\:rating_2").attr("src", "resources/images/star.ico");
		$("#review-form\\:rating_1").attr("src", "resources/images/star.ico");
	}
});

$("#review-form\\:rating_2").click(function() {
	$(this).data('clicked', true);

	$("#review-form\\:rating_1").data('clicked', false);
	$("#review-form\\:rating_3").data('clicked', false);
	$("#review-form\\:rating_4").data('clicked', false);
	$("#review-form\\:rating_5").data('clicked', false);

	$(this).attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_1").attr("src", "resources/images/star.ico");
});
$("#review-form\\:rating_2").mouseover(function() {
	$(this).attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_1").attr("src", "resources/images/star.ico");

	$("#review-form\\:rating_3").attr("src", "resources/images/grey-star.ico");
	$("#review-form\\:rating_4").attr("src", "resources/images/grey-star.ico");
	$("#review-form\\:rating_5").attr("src", "resources/images/grey-star.ico");
}).mouseout(
		function() {
			if ($(this).data("clicked") == false
					|| $(this).data("clicked") == null) {
				$(this).attr("src", "resources/images/grey-star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/grey-star.ico");
			}
			if ($("#review-form\\:rating_5").data("clicked") == true) {
				$("#review-form\\:rating_5").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_4").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_3").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_2").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
			if ($("#review-form\\:rating_4").data("clicked") == true) {
				$("#review-form\\:rating_4").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_3").attr("src",
						"resources/images/star.ico");
				// Written by Sean-Frankel Gaon Canlas
				$("#review-form\\:rating_2").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
			if ($("#review-form\\:rating_3").data("clicked") == true) {
				$("#review-form\\:rating_3").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_2").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
			if ($("#review-form\\:rating_1").data("clicked") == true) {
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
		});

$("#review-form\\:rating_3").click(function() {
	$(this).data('clicked', true);

	$("#review-form\\:rating_1").data('clicked', false);
	$("#review-form\\:rating_2").data('clicked', false);
	$("#review-form\\:rating_4").data('clicked', false);
	$("#review-form\\:rating_5").data('clicked', false);

	$(this).attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_2").attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_1").attr("src", "resources/images/star.ico");
});
$("#review-form\\:rating_3").mouseover(function() {
	$(this).attr("src", "./resources/images/star.ico");
	$("#review-form\\:rating_2").attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_1").attr("src", "resources/images/star.ico");

	$("#review-form\\:rating_4").attr("src", "resources/images/grey-star.ico");
	$("#review-form\\:rating_5").attr("src", "resources/images/grey-star.ico");
}).mouseout(
		function() {
			if ($(this).data("clicked") == false
					|| $(this).data("clicked") == null) {
				$(this).attr("src", "resources/images/grey-star.ico");
				// Written by Sean-Frankel Gaon Canlas
				$("#review-form\\:rating_2").attr("src",
						"resources/images/grey-star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/grey-star.ico");
			}
			if ($("#review-form\\:rating_5").data("clicked") == true) {
				$("#review-form\\:rating_5").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_4").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_3").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_2").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
			if ($("#review-form\\:rating_4").data("clicked") == true) {
				$("#review-form\\:rating_4").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_3").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_2").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
			if ($("#review-form\\:rating_2").data("clicked") == true) {
				$("#review-form\\:rating_2").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
			if ($("#review-form\\:rating_1").data("clicked") == true) {
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
		});

$("#review-form\\:rating_4").click(function() {
	$(this).data('clicked', true);

	$("#review-form\\:rating_1").data('clicked', false);
	$("#review-form\\:rating_2").data('clicked', false);
	$("#review-form\\:rating_3").data('clicked', false);
	$("#review-form\\:rating_5").data('clicked', false);

	$(this).attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_3").attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_2").attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_1").attr("src", "resources/images/star.ico");
});
$("#review-form\\:rating_4").mouseover(function() {
	$(this).attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_3").attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_2").attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_1").attr("src", "resources/images/star.ico");

	$("#review-form\\:rating_5").attr("src", "resources/images/grey-star.ico");
}).mouseout(
		function() {
			if ($(this).data("clicked") == false
					|| $(this).data("clicked") == null) {
				$(this).attr("src", "resources/images/grey-star.ico");
				$("#review-form\\:rating_3").attr("src",
						"resources/images/grey-star.ico");
				$("#review-form\\:rating_2").attr("src",
						"resources/images/grey-star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/grey-star.ico");
			}
			if ($("#review-form\\:rating_5").data("clicked") == true) {
				$("#review-form\\:rating_5").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_4").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_3").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_2").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
			if ($("#review-form\\:rating_3").data("clicked") == true) {
				$("#review-form\\:rating_3").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_2").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
			if ($("#review-form\\:rating_2").data("clicked") == true) {
				$("#review-form\\:rating_2").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
			if ($("#review-form\\:rating_1").data("clicked") == true) {
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
		});

$("#review-form\\:rating_5").click(function() {
	$(this).data('clicked', true);

	$("#review-form\\:rating_1").data('clicked', false);
	$("#review-form\\:rating_2").data('clicked', false);
	$("#review-form\\:rating_3").data('clicked', false);
	$("#review-form\\:rating_4").data('clicked', false);

	$(this).attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_4").attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_3").attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_2").attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_1").attr("src", "resources/images/star.ico");
});
$("#review-form\\:rating_5").mouseover(function() {
	$(this).attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_4").attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_3").attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_2").attr("src", "resources/images/star.ico");
	$("#review-form\\:rating_1").attr("src", "resources/images/star.ico");
}).mouseout(
		function() {
			if ($(this).data("clicked") == false
					|| $(this).data("clicked") == null) {
				// Written by Sean-Frankel Gaon Canlas
				$(this).attr("src", "resources/images/grey-star.ico");
				$("#review-form\\:rating_4").attr("src",
						"resources/images/grey-star.ico");
				$("#review-form\\:rating_3").attr("src",
						"resources/images/grey-star.ico");
				$("#review-form\\:rating_2").attr("src",
						"resources/images/grey-star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/grey-star.ico");
			}
			if ($("#review-form\\:rating_4").data("clicked") == true) {
				$("#review-form\\:rating_4").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_3").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_2").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
			if ($("#review-form\\:rating_3").data("clicked") == true) {
				$("#review-form\\:rating_3").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_2").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
			if ($("#review-form\\:rating_2").data("clicked") == true) {
				$("#review-form\\:rating_2").attr("src",
						"resources/images/star.ico");
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
			if ($("#review-form\\:rating_1").data("clicked") == true) {
				$("#review-form\\:rating_1").attr("src",
						"resources/images/star.ico");
			}
		});