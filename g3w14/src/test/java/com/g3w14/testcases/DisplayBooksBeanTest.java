package com.g3w14.testcases;

import static org.junit.Assert.*;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.g3w14.action.DisplayBooksBean;
import com.g3w14.data.BookBean;

/**
 * JUnit testing class for DisplayBooksBean.
 * 
 * @author Tyler Patricio
 *
 */
@RunWith(Arquillian.class)
public class DisplayBooksBeanTest {
	@Inject
	DisplayBooksBean dbb;
	
	BookBean bb;
	
	@Deployment
	public static WebArchive deploy()
	{
		final File[] dependencies = Maven
				.resolver()
				.loadPomFromFile("pom.xml")
				.resolve("mysql:mysql-connector-java",
						"org.assertj:assertj-core").withoutTransitivity()
				.asFile();

		final WebArchive webArchive = ShrinkWrap.create(WebArchive.class)
				.addPackage(DisplayBooksBean.class.getPackage())
				.addPackage(BookBean.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource("resources-mysql-ds.xml", "resources.xml")
				.addAsLibraries(dependencies);

		return webArchive;
	}
	
	@Before
	public void init() throws SQLException
	{
		bb = new BookBean();
	}

	/**
	 * Test method for {@link com.g3w14.action.DisplayBooksBean#retrieveBooksForBanner()}.
	 */
//	@Test
//	public void testRetrieveBooksForBanner() {
//		fail("Not yet implemented");
//	}

	/**
	 * Test method for {@link com.g3w14.action.DisplayBooksBean#retrieveBooksByGenre(java.lang.String)}.
	 * @throws SQLException 
	 */
	/*
	@Test
	public void testRetrieveBooksByGenre() throws SQLException {
		ArrayList<BookBean> list = new ArrayList<>();
		dbb.setBooksByGenre("Fiction");
		

		list = dbb.getBooksByGenre(list.get(0).getGenre());
		

		assertEquals("The number [] is expected here.", 123, list.size());
	}
	*/

	/**
	 * Test method for {@link com.g3w14.action.DisplayBooksBean#retrieveSingleBook(java.lang.String)}.
	 */
//	@Test
//	public void testRetrieveSingleBook() {
//		fail("Not yet implemented");
//	}

	/**
	 * Test method for {@link com.g3w14.action.DisplayBooksBean#retrievePreviousBookInterests(com.g3w14.data.ClientBean)}.
	 */
//	@Test
//	public void testRetrievePreviousBookInterests() {
//		fail("Not yet implemented");
//	}
}