package com.g3w14.testcases;

import static org.junit.Assert.*;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.g3w14.data.SurveyBean;
import com.g3w14.persistence.SurveyDAOImp;

/**
 * JUnit testing cases for SurveyDAOImp class.
 * 
 * @author 1040134 Jhonatan Orjuela
 *
 */
@RunWith(Arquillian.class)
public class SurveyDAOImpTest {

	@Inject
	SurveyDAOImp sdao;
	
	SurveyBean sB;
	
	@Deployment
	public static WebArchive deploy()
	{
		final File[] dependencies = Maven
				.resolver()
				.loadPomFromFile("pom.xml")
				.resolve("mysql:mysql-connector-java",
						"org.assertj:assertj-core").withoutTransitivity()
				.asFile();

		final WebArchive webArchive = ShrinkWrap.create(WebArchive.class)
				.addPackage(SurveyDAOImp.class.getPackage())
				.addPackage(SurveyBean.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource("resources-mysql-ds.xml", "resources.xml")
				.addAsLibraries(dependencies);

		return webArchive;
	}
	
	@Before
	public void init() throws SQLException
	{
		sB = new SurveyBean();
	}

	@Test(timeout = 5000)
	public void successfullInsertRecord() throws SQLException {
		sB.setQuestion("What is Love?");
		sB.setOption1("baby don't hurt me,");
		sB.setOption2("don't hurt me,");
		sB.setOption3("no more.");
		sB.setOption4("much impress, such laughs!");
		
		sB.setVotes1(3);
		sB.setVotes2(1);
		sB.setVotes3(8);
		sB.setVotes4(10);
		
		
		sB.setTotalVotes(22);
		assertEquals("expected 1 :",1 ,sdao.insertSurvey(sB));
		
	}
	
	@Test//(timeout = 5000)
	public void successfullExtraction() throws SQLException {
		ArrayList<SurveyBean> results = sdao.getSurveyRecords();
		assertEquals("expected 1 :", 1, results.size());
	}
	
	@Test//(timeout = 5000)
	public void successfullSingleExtraction() throws SQLException  {
		sB = sdao.getSingleSurvey(1);
		System.out.println(sB.getQuestion());
		assertEquals("expected 1 :", 1 , sB.getSurveyId());
	}
	
	@Test//(timeout = 5000)
	public void successfullDeletion() throws SQLException{
		sB.setSurveyId(15);
		assertEquals("expected 1 :", 1 ,sdao.deleteSurvey(sB));
	}
}