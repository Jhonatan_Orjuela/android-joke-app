package com.g3w14.testcases;

import static org.junit.Assert.*;

import java.io.File;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.g3w14.data.BookBean;
import com.g3w14.persistence.BookDAOImp;

/**
 * JUnit testing cases for BookDAOImp class.
 * 
 * @author Tyler Patricio
 */
@RunWith(Arquillian.class)
public class BookDAOImpTest
{
	@Inject
	private BookDAOImp bdi;
	
	// data beans used
	private BookBean bb;
	private BookBean bb2;
	
	private ArrayList<BookBean> bbList;
	private GregorianCalendar gc;
	
	@Deployment
	public static WebArchive deploy()
	{
		final File[] dependencies = Maven
				.resolver()
				.loadPomFromFile("pom.xml")
				.resolve("mysql:mysql-connector-java",
						"org.assertj:assertj-core").withoutTransitivity()
				.asFile();

		final WebArchive webArchive = ShrinkWrap.create(WebArchive.class)
				.addPackage(BookDAOImp.class.getPackage())
				.addPackage(BookBean.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource("resources-mysql-ds.xml", "resources.xml")
				.addAsLibraries(dependencies);

		return webArchive;
	}

	@Before
	public void init() throws SQLException
	{
		bb = new BookBean();
		bb2 = new BookBean();
		bbList = new ArrayList<BookBean>();
		gc = new GregorianCalendar();

		bb.setIsbn10("isbn10");
		bb.setIsbn13("isbn13");
		bb.setTitle("Title");
		bb.setAuthor("Author");
		bb.setPublisher("Publisher");
		bb.setNumOfPages(100);
		bb.setGenre("Genre");
		bb.setBookCoverImg("BookCoverImage.png");
		bb.setBookType(3);
		bb.setEbookFormat("EbookFormat");
		bb.setNumOfCopies(0);
		// bb.setWholeSalePrice(new BigDecimal(4.99));
		// bb.setListPrice(new BigDecimal(9.99));
		// bb.setSalePrice(new BigDecimal(0.00));
		bb.setWeight(0);
		bb.setDimensions("N/A");
		bb.setRemovalStatus(0);
		bb.setDateEntered(new Timestamp(gc.getTimeInMillis()));
		bb2.setAuthor("Larry McMutry");
		bb2.setGenre("Western");
	}

	/**
	 * Test method for
	 * {@link com.g3w14.persistence.BookDAOImp#returnAllRecords()}.
	 */
	@Test//(timeout = 2000)
	public void testReturnAllRecords() throws SQLException
	{
		bbList = bdi.getAllRecords();

		// check current records amount
		assertEquals("Total records in database: 104", 104, bbList.size());
	}

	/**
	 * Test method for
	 * {@link com.g3w14.persistence.BookDAOImp#advancedSearch(com.g3w14.data.BookBean)}
	 * .
	 */
	@Test//(timeout = 2000)
	public void testAdvancedSearch() throws SQLException
	{
		ArrayList<BookBean> bbList2 = bdi.advancedSearch(bb2);

		assertEquals("Expected book title: Lonesome Dove: A Novel", "Lonesome Dove: A Novel",
				bbList2.get(0).getTitle());
	}

	/**
	 * Test method for                                         
	 * {@link com.g3w14.persistence.BookDAOImp#deleteRecord(String)}.
	 * 
	 * @throws SQLException
	 */
	@Test//(timeout = 2000)
	public void testDeleteRecord() throws SQLException
	{
		int resultNum;

		// delete the third record
		resultNum = bdi.deleteRecord("isbn13");

		assertEquals("The number 1 is expected here:", 1, resultNum);
	}

	/**
	 * Test method for
	 * {@link com.g3w14.persistence.BookDAOImp#deleteRecord(String)}.
	 * 
	 * @throws SQLException
	 */
	@Test//(timeout = 2000)
	public void testDeleteNonExistantRecord() throws SQLException
	{
		int resultNum;

		// delete the third record
		resultNum = bdi.deleteRecord("Hello");

		assertEquals("The number 0 is expected here:", 0, resultNum);
	}

	/**
	 * Test method for
	 * {@link com.g3w14.persistence.BookDAOImp#deleteRecord(String)}.
	 * 
	 * @throws SQLException
	 */
	@Test//(timeout = 2000)
	public void testDeleteWithNull() throws SQLException
	{
		int resultNum;

		// delete the third record
		resultNum = bdi.deleteRecord(null);

		assertEquals("The number 0 is expected here:", 0, resultNum);
	}

	/**
	 * Test method for
	 * {@link com.g3w14.persistence.BookDAOImp#insertRecord(com.g3w14.data.BookBean)}.
	 */
	@Test//(timeout = 2000)
	public void testInsertRecord() throws SQLException
	{
		int resultNum;

		// insert the test record
		resultNum = bdi.insertRecord(bb);

		assertEquals("The number 1 is expected here:", 1, resultNum);
	}

	/**
	 * Test method for
	 * {@link com.g3w14.persistence.BookDAOImp#insertRecord(com.g3w14.data.BookBean)}
	 * .
	 */
	@Ignore
	@Test(/*timeout = 2000, */expected = NullPointerException.class)
	public void testInsertRecordWithNull() throws SQLException
	{
		@SuppressWarnings("unused")
		int resultNum;

		// insert the test record
		resultNum = bdi.insertRecord(null);
	}

	/**
	 * Test method for
	 * {@link com.g3w14.persistence.BookDAOImp#updateRecord(com.g3w14.data.BookBean)}
	 * .
	 */
	@Test//(timeout = 2000)
	public void testUpdateRecord() throws SQLException
	{
		int resultNum;

		bbList = bdi.getAllRecords();
		bbList.get(0).setAuthor("Tyler Patricio");

		resultNum = bdi.updateRecord(bbList.get(0));
		assertEquals("The number 1 is expected here:", 1, resultNum);
	}

	/**
	 * Test method for
	 * {@link com.g3w14.persistence.BookDAOImp#updateRecord(com.g3w14.data.BookBean)}
	 * .
	 */
	@Test(/*timeout = 2000, */expected = NullPointerException.class)
	public void testUpdateRecordWithNull() throws SQLException
	{
		// update with null
		@SuppressWarnings("unused")
		int resultNum = bdi.updateRecord(null);
	}
}